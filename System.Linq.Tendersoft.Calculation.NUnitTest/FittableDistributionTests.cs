﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;

namespace System.Linq.Tendersoft.Calculation.NUnitTest
{
  [TestFixture]
  public class FittableDistributionTests
  {
    [Test]
    [Description("Test FIFO addition of elements")]
    public void TestElementAddingInFifo()
    {
      var testData = new double[] { 
        0, -0.00077, 0.00116, 0.00267, -0.00202, 0.00226, 0.00053, 0.00566, 0.00064, -0.00519, };/*
        0.00286, 0.00183, 0.00135, -0.00705, 0.00595, -0.00159, 0.00294, 0.00248, -0.00314, -0.00226, 
          -0.0029, -0.00129, -0.00435, 0.0065, -0.00246, 0.00237, -0.00304, 0.00131, 0.00121, -0.00421, 
            -0.00218, 0.00122, -0.00057, 0.00461, 0.00145, 0.00166, -0.0009, -0.00115, -0.00063, 0.00927, 
              -0.00862, -0.00126, 0.00066, 0.00505, -0.00111, 0.00072, 0.00261, 0.00091, -0.00598, 0.00113, 
      0.00079, -0.00194, -0.00138, 0.00535, 0.00081, 0.00043, -0.00136, -0.00202, -0.00294, 0.00453, 
        -0.0004, 0.00085, 0.00242, 0.00069, -0.00014, 0.00057, -0.00287, 0.00082, -0.00171, 0.00373, 
          -0.00061, 0.00109, -0.00089, -0.0014, -0.00883, 0.0063, -0.00969, 0.00912, -0.00264, 0.00293, 
            -0.00638, 0.00533, 5E-05, 0.00435, -0.00655, -0.00436, 0.00564, 0.00098, 7E-05, -0.0056, 
      0.00453, -0.00443, -0.003, 0.00239, 0.00547, 0.00055, -0.00412, -0.00462, 0.00165, 0.00075, 
      0.00601, -0.00312, -0.00511, -0.00021, };
      */
      var fdist = new FittableDistribution(testData);
      Assert.AreEqual(0, fdist.CDFData.First().Item2);
      var x0Diff = Math.Abs(fdist.CDFData.First().Item1 - 1.33 * -0.00519);
      Assert.LessOrEqual(x0Diff, 0.0001);

      fdist.Add(0.00286, true);
      fdist.Add(0.00183, true);
      fdist.Add(0.00135, true);
      fdist.Add(-0.00705, true);
      Assert.AreEqual(0, fdist.CDFData.First().Item2);
      var x1Diff = Math.Abs(fdist.CDFData.First().Item1 - 1.33 * -0.00705);
      Assert.LessOrEqual(x1Diff, 0.0001);
    }

    [Test]
    [Description("Tests Mean calculation")]
    public void TestMean()
    {
      var testData = new double[] { 
        0, -0.00077, 0.00116, 0.00267, -0.00202, 0.00226, 0.00053, 0.00566, 0.00064, -0.00519, };
      var fdist = new FittableDistribution(testData);
      var diff = Math.Abs(fdist.Mean - 0.00122814159292036);

      // equal to specified precision
      Assert.LessOrEqual(diff, fdist.Mean / 10e4);
    }

    [Test]
    [Description("Tests CDF at max X")]
    public void TestCDFAtMaxX()
    {
      var testData = new double[] { 
        0, -0.00077, 0.00116, 0.00267, -0.00202, 0.00226, 0.00053, 0.00566, 0.00064, -0.00519, };
      var fdist = new FittableDistribution(testData);

      Assert.AreEqual(fdist.CDFAt(0.00566), 1);
    }

    [Test]
    [Description("Tests CDF at min X")]
    public void TestCDFAtMinX()
    {
      var testData = new double[] { 
        0, -0.00077, 0.00116, 0.00267, -0.00202, 0.00226, 0.00053, 0.00566, 0.00064, -0.00519, };
      var fdist = new FittableDistribution(testData);
      var y = fdist.CDFAt(-0.00519);
      var val = Math.Abs(y - 0.248325358851675);
      Assert.LessOrEqual(val, 1/10e4);
    }

    [Test]
    [Description("Tests InvertedCDF at max X")]
    public void TestInvertedCDFAtMaxX()
    {
      var testData = new double[] { 
        0, -0.00077, 0.00116, 0.00267, -0.00202, 0.00226, 0.00053, 0.00566, 0.00064, -0.00519, };
      var fdist = new FittableDistribution(testData);
      var val = fdist.InvertedCDFAt(1);

      Assert.AreEqual(0.00566, val);
    }

    [Test]
    [Description("Tests InvertedCDF at min X")]
    public void TestInvertedCDFAtMinX()
    {
      var testData = new double[] { 
        0, -0.00077, 0.00116, 0.00267, -0.00202, 0.00226, 0.00053, 0.00566, 0.00064, -0.00519, };
      var fdist = new FittableDistribution(testData);

      Assert.AreEqual(-0.00519 * 1.33, fdist.InvertedCDFAt(0));
    }

    //[Test]
    //[Description("Tests InvertedCDF at tolerance interval")]
    //public void TestInvertedCDFAtToleranceInterval()
    //{
    //  var testData = new double[] { 
    //    0, -0.00077, 0.00116, 0.00267, -0.00202, 0.00226, 0.00053, 0.00566, 0.00064, -0.00519, };
    //  var fdist = new FittableDistribution(testData);

    //  var r = new Random();
    //  var testCount = 10;
    //  var increment = (fdist.InvertedCDFData.First().Item1+fdist.InvertedCDFData.Last().Item1)
    //    /(double)testCount;
    //  for (int i = 0; i < 10; i++)
    //  {
    //    var testValue = fdist.InvertedCDFData.First().Item1+i*increment;
    //    var leftVal = fdist.CalculateValueAtToleranceInterval(testValue, FittableDistribution.CalculationDirection.Up);
    //    var rightVal = fdist.CalculateValueAtToleranceInterval(testValue, FittableDistribution.Boundary.Left);
    //    Assert.AreEqual(leftVal, rightVal);

    //    leftVal = fdist.CalculateValueAtToleranceInterval(testValue, FittableDistribution.CalculationDirection.Down);
    //    rightVal = fdist.CalculateValueAtToleranceInterval(testValue, FittableDistribution.Boundary.Right);
    //    Assert.AreEqual(leftVal, rightVal);
    //  }

    //}
  }
}
