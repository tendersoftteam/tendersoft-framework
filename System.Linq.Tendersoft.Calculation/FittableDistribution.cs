﻿// -----------------------------------------------------------------------
// <copyright file="FittableDistribution.cs" company="">
// TODO: Update copyright text.
// </copyright>
// -----------------------------------------------------------------------

namespace System.Linq.Tendersoft.Calculation
{
  using System;
  using System.Collections.Generic;
  using System.Diagnostics.Contracts;
  using System.Linq;

  /// <summary>
  /// TODO: Update summary.
  /// </summary>
  public class FittableDistribution
  {
    public FittableDistribution(IEnumerable<double> data)
    {
      Contract.Requires<ArgumentNullException>(data != null);
      Contract.Requires<ArgumentException>(data.Count() >= 2);
      Contract.Ensures(Contract.ForAll(CDFData.SlidingWindow(2), x => x[0].Item1 <= x[1].Item1), "X values are not non-decreasing");
      Contract.Ensures(Contract.ForAll(CDFData.SlidingWindow(2), x => x[0].Item2 <= x[1].Item2), "Y values are not non-decreasing");

      _data = data.ToList();
      Initialize();
    }
    public FittableDistribution(IEnumerable<decimal> data)
      : this(data.Select(x => (double)x))
    {
      Contract.Requires<ArgumentNullException>(data != null);
      Contract.Requires<ArgumentException>(data.Count() >= 2);
      Contract.Ensures(Contract.ForAll(CDFData.SlidingWindow(2), x => x[0].Item1 <= x[1].Item1), "X values are not non-decreasing");
      Contract.Ensures(Contract.ForAll(CDFData.SlidingWindow(2), x => x[0].Item2 <= x[1].Item2), "Y values are not non-decreasing");
    }

    public void Add(double value, bool removeFirstElement = false)
    {
      Contract.Ensures(Contract.ForAll(CDFData.SlidingWindow(2), x => x[0].Item1 <= x[1].Item1), "X values are not non-decreasing");
      Contract.Ensures(Contract.ForAll(CDFData.SlidingWindow(2), x => x[0].Item2 <= x[1].Item2), "Y values are not non-decreasing");
      if (removeFirstElement)
      {
        _data.RemoveAt(0);
      }
      _data.Add(value);
      Initialize();
    }
    public void Add(decimal value, bool removeFirstElement = false)
    {
      Add((double)value, removeFirstElement);
    }

    private void Initialize()
    {
      Contract.Requires<ArgumentNullException>(_data != null);
      Contract.Requires<ArgumentException>(_data.Count() >= 2);
      Contract.Ensures(Contract.ForAll(_CDFData.SlidingWindow(2), x => x[0].Item1 <= x[1].Item1), "X values are not non-decreasing");
      Contract.Ensures(Contract.ForAll(_CDFData.SlidingWindow(2), x => x[0].Item2 <= x[1].Item2), "Y values are not non-decreasing");

      var dataOrdered = _data
        .OrderBy(x => x)
        .ToList();

      //var ci = new System.Globalization.CultureInfo("en-US");
      //var str = _data.Aggregate(new System.Text.StringBuilder(), (sb, x) => sb.AppendFormat(ci,"{0:R}, ",x), sb => sb.ToString());

      var cdfSums = dataOrdered
        .Scan0(0.0, (sum, x) => sum + Math.Abs(x))
        .ToList();

      var preCDFFata = new double[] { dataOrdered.First() * 1.33 } // set x to use in CDF(x) = 0
        .Concat(dataOrdered)
        .ToList();
      Contract.Assert(preCDFFata.Count() == cdfSums.Count(), "pre-Zip sequences have non-equal counts");

      _CDFData = preCDFFata
        .Zip(cdfSums, (st, cdf) => Tuple.Create(st, cdf / cdfSums.Last()))
        .ToList();

      Contract.Assert(_CDFData.First().Item2 == 0, "CDF normalization is invalid: y[0] != 0");
      Contract.Assert(_CDFData.Last().Item2 == 1, "CDF normalization is invalid: y[n] != 1");
#if DEBUG
      var failsX = _CDFData.SlidingWindow(2).Where(x => !(x[0].Item1 <= x[1].Item1));
      if (failsX.Any())
      {
        var first = failsX.First();
        var result = first[0].Item1 <= first[1].Item1;
        Console.WriteLine("X values are not non-decreasing {0} times.", failsX.Count());
      }

      var failsY = _CDFData.SlidingWindow(2).Where(x => !(x[0].Item2 <= x[1].Item2));
      if (failsY.Any())
      {
        Console.WriteLine("Y values are not non-decreasing {0} times.", failsY.Count());
      }
#endif
    }

    private List<Tuple<double, double>> _CDFData;
    private List<double> _data;
    public IEnumerable<Tuple<double, double>> CDFData
    {
      get
      {
        return _CDFData
          .AsEnumerable()
          .Memoize();
      }
    }
    public IEnumerable<Tuple<double, double>> InvertedCDFData
    {
      get
      {
        return _CDFData
          .Select(x => Tuple.Create(x.Item2, x.Item1))
          .Memoize();
      }
    }

    //public IEnumerable<Tuple<decimal, decimal>> PDFData
    //{
    //  get
    //  {
    //    var scaleFactor = 1000000000; // used to get rid of rounding errors for very small numbers
    //    var yn_1 = decimal.Zero;
    //    foreach (var item in _CDFData.SlidingWindow(2))
    //    {
    //      var x0 = item[0].Item1 * scaleFactor;
    //      var x1 = item[1].Item1 * scaleFactor;
    //      var y0 = item[0].Item2 * scaleFactor;
    //      var y1 = item[1].Item2 * scaleFactor;

    //      var deltaX = x1 - x0;
    //      Contract.Assert(deltaX >= 0);
    //      if (deltaX == decimal.Zero)
    //      {
    //        yield return Tuple.Create(item[1].Item1, yn_1 / scaleFactor);
    //        continue;
    //      }
    //      var deltaP = y1 - y0;
    //      Contract.Assert(deltaP >= decimal.Zero);
    //      Contract.Assert(deltaP / scaleFactor <= 1M);
    //      Contract.Assert(yn_1 * deltaX >= decimal.Zero);
    //      var yn = (2 * deltaP - yn_1 * deltaX) / deltaX;
    //      if (yn < decimal.Zero)
    //      {
    //        yn = decimal.Zero;
    //      }
    //      yield return Tuple.Create(item[1].Item1, yn / scaleFactor);
    //      yn_1 = yn;
    //    }
    //  }
    //}

    //private decimal[] _toleranceIntervals = new decimal[] { .66M, .95M, .997M };

    public double Mean
    {
      get
      {
        var y0 = 0.5;

        Contract.Assume(this._CDFData != null);
        Contract.Assume(this._CDFData.Count >= 2);

        var exactPairs = _CDFData.Where(p => p.Item2 == y0);
        // doubles are rarely exactly equal to one another so this will likely not happen
        if (exactPairs.Any())
        {
          return exactPairs.First().Item1;
        }

        var pair = _CDFData.SlidingWindow(2)
          .TakeWhile(x => x[0].Item2 <= y0)
          .Last();

        var invertedPair = pair
          .Select(p => Tuple.Create(p.Item2, p.Item1))
          .ToArray();

        var vap = Statistic.EvaluateFunctionBetweenPoints(invertedPair[0], invertedPair[1], y0);

        return vap;
      }
    }

    /// <summary>
    /// Calculates boundaries of source values that map to probabilities within specified tolerance interval:
    /// F(toleranceInterval)={CDF^-1(Mean-toleranceInterval/2), CDF^-1(Mean+toleranceInterval/2)}
    /// </summary>
    /// <param name="toleranceInterval">[0;1]</param>
    /// <returns>Tuple containing Item1=Lower and Item2=Higher range boundaries</returns>
    public Tuple<double, double> CalculateValueRangeForToleranceInterval(double toleranceInterval
#if DEBUG
, bool stopRecursion = false
#endif
)
    {
      Contract.Requires<ArgumentException>(toleranceInterval >= 0);
      Contract.Requires<ArgumentException>(toleranceInterval <= 1);
      Contract.Ensures(Contract.Result<Tuple<double, double>>().Item1 >= _CDFData.Min().Item1);
      Contract.Ensures(Contract.Result<Tuple<double, double>>().Item2 <= _CDFData.Max().Item1);

      if (toleranceInterval == 0
#if DEBUG
 && stopRecursion == false
#endif
)
      {
#if DEBUG
        var noTolerance = CalculateValueRangeForToleranceInterval(0, true);
        Contract.Assert(noTolerance.Item1 == noTolerance.Item2, "Value ranges for toleranceInterval=0 are not equal.");
        Contract.Assert(noTolerance.Item1 == Mean && Mean == noTolerance.Item2, "Value ranges for toleranceInterval=0 are not equal to Mean.");
#endif
        return Tuple.Create(Mean, Mean);
      }
      if (toleranceInterval == 1)
      {
        return Tuple.Create(_CDFData.First().Item1, _CDFData.Last().Item1);
      }

      var minProbability = .5 - toleranceInterval / 2.0;
      var maxProbability = .5 + toleranceInterval / 2.0;
      var lowerBound = CalculateValueAtToleranceInterval(minProbability, CalculationDirection.Up);
      var upperBound = CalculateValueAtToleranceInterval(maxProbability, CalculationDirection.Down);

      return Tuple.Create(lowerBound, upperBound);
    }

    public enum CalculationDirection
    {
      Up,  // from small to big numbers
      Down // from big to small numbers
    }

    //public enum Boundary
    //{
    //  Left,
    //  Right
    //}

    //public double CalculateValueAtToleranceInterval(double toleranceInterval, Boundary boundary = Boundary.Left)
    //{
    //  switch (boundary)
    //  {
    //    case Boundary.Left:
    //      return InvertedCDFAt(toleranceInterval);
    //    case Boundary.Right:
    //      return InvertedCDFData.Reverse().CalculateValueAtX(toleranceInterval);
    //    default:
    //      throw new NotSupportedException("Unspecified boundary is not supported.");
    //  }
    //}

    public double CalculateValueAtToleranceInterval(double toleranceInterval, CalculationDirection calculationDirection = CalculationDirection.Up)
    {
      Contract.Requires<ArgumentException>(toleranceInterval >= 0);
      Contract.Requires<ArgumentException>(toleranceInterval <= 1);
      Contract.Ensures(Contract.Result<double>() >= _CDFData.Min().Item1);
      Contract.Ensures(Contract.Result<double>() <= _CDFData.Max().Item1);

      var y0 = toleranceInterval;
      Tuple<double, double>[] pair = null;

      Contract.Assume(this._CDFData != null);
      Contract.Assume(this._CDFData.Count >= 2);

      switch (calculationDirection)
      {
        case CalculationDirection.Up:
          pair = _CDFData
            .SlidingWindow(2)
            .TakeWhile(x => x[0].Item2 <= y0)
            .Last();
          break;

        case CalculationDirection.Down:
          pair = _CDFData
            .AsEnumerable()
            .Reverse()
            .SlidingWindow(2)
            .TakeWhile(x => x[0].Item2 >= y0)
            .Last()
            .Reverse()
            .ToArray();
          break;

        default:
          throw new NotSupportedException();
      }

      for (int i = 0; i < pair.Length; i++)
      {
        if (Math.Abs(pair[i].Item2 - y0) <= double.Epsilon)
        {
          return pair[i].Item1;
        }
      }

      var invPair = pair
        .Select(p => Tuple.Create(p.Item2, p.Item1))
        .ToArray();
      var vap = Statistic.EvaluateFunctionBetweenPoints(invPair[0], invPair[1], y0);

      return vap;
    }

    public double CDFAt(double x)
    {
      Contract.Ensures(Contract.Result<double>() >= 0);
      Contract.Ensures(Contract.Result<double>() <= 1);

      return CDFData.CalculateValueAtX(x);
    }

    public double InvertedCDFAt(double x)
    {
      Contract.Requires<ArgumentException>(x >= 0);
      Contract.Requires<ArgumentException>(x <= 1);

      return InvertedCDFData.CalculateValueAtX(x);
    }

    public IEnumerable<Tuple<double, double>> GeneratePDFData(int resolution)
    {
      var minX = CDFData.First().Item1;
      var maxX = CDFData.Last().Item1;
      var increment = (Math.Abs(minX) + Math.Abs(maxX)) / (double)resolution;

      var x = minX;
      for (int i = 0; i < resolution && x <= maxX; i++)
      {
        var y = CDFAt(x);
        yield return Tuple.Create(x, y);

        x += increment;
      }
    }
    public double PDFAt(double x) { throw new NotImplementedException(); }

    public double QGuassianDistribution(double x, double y, double q, double ro)
    {
      Contract.Requires<ArgumentException>(1 < q);
      Contract.Requires<ArgumentException>(q < 3);

      var euclideanNorm = Math.Sqrt(x * x + y * y);
      var exp = 1 / (1 - q);
      var expBase = 1 + (q - 1) / ((3 - q) * ro * ro) * euclideanNorm * euclideanNorm;

      var qGuassian = Math.Pow(expBase, exp);
      return qGuassian;
    }

    public Tuple<double, double> RandomQGuassian() 
    {
      var r = new Random();
      var u1 = r.NextDouble();
      var u2 = r.NextDouble();

      var z1 = Math.Sqrt(-2 * Math.Log(u1)) * Math.Cos(2 * Math.PI * u2);
      var z2 = Math.Sqrt(-2 * Math.Log(u1)) * Math.Sin(2 * Math.PI * u2);

      return Tuple.Create(z1, z2);
    }
  }

  internal static class SupportTools
  {
    internal static double CalculateValueAtX(this IEnumerable<Tuple<double, double>> data, double x)
    {
      Contract.Requires<ArgumentNullException>(data != null);
      Contract.Requires<ArgumentException>(data.Count() >= 2);
      Contract.Requires<ArgumentException>(data
            .SlidingWindow(2)
            .Any(ps => ps[0].Item1 <= x && x <= ps[1].Item1));
      Contract.Ensures(data
            .SlidingWindow(2)
            .Any(ps =>
              ps[0].Item2 <= Contract.Result<double>()
              && Contract.Result<double>() <= ps[1].Item2));

      var pair = data
            .SlidingWindow(2)
            .TakeWhile(p => p[0].Item1 <= x)
            .Last();

      // there is a low probability of values being exactly equal
      for (int i = 0; i < pair.Length; i++)
      {
        if (Math.Abs(pair[i].Item1 - x) <= double.Epsilon)
        {
          return pair[i].Item2;
        }
      }

      return Statistic.EvaluateFunctionBetweenPoints(pair[0], pair[1], x);
    }
  }
}