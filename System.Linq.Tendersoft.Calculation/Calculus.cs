﻿namespace System.Linq.Tendersoft.Calculation
{
  using System.Collections.Generic;
  using System.Diagnostics.Contracts;
  using MiscUtil;

  public static class Calculus
  {
    /// <summary>
    /// Calculates derivatives for supplied Y series. X series is created on the fly with deltaX=1
    /// </summary>
    /// <param name="series"></param>
    /// <returns></returns>
    /// (f(x2)-f(x1))/(x2-x1)
    public static IEnumerable<double> Derivate(this IEnumerable<double> series)
    {
      Contract.Requires<ArgumentNullException>(series != null);
      Contract.Ensures(Contract.Result<IEnumerable<double>>().Count() == series.Count() - 1);

      return series
        .Select((y, i) => new double[] { i, y })
        .Derivate()
        .Select(s => s[1]);
    }

    /// <summary>
    ///
    /// </summary>
    /// <param name="series"></param>
    /// <returns></returns>
    /// (f(x2)-f(x1))/(x2-x1)
    public static IEnumerable<double[]> Derivate(this IEnumerable<double[]> series)
    {
      Contract.Requires<ArgumentNullException>(series != null);
      Contract.Requires<ArgumentException>(Contract.ForAll(series, s => s.Length >= 2));
      Contract.Requires<ArgumentException>(Contract.ForAll(series.SlidingWindow(2), s => s[1][0] > s[0][0]));
      Contract.Requires<DivideByZeroException>(Contract.ForAll(series.SlidingWindow(2), s => s[1][0] - s[0][0] >= double.Epsilon));
      Contract.Ensures(Contract.Result<IEnumerable<double[]>>().Count() == series.Count() - 1);
      Contract.Ensures(Contract.ForAll(Contract.Result<IEnumerable<double[]>>(), o => o.Count() == 2));

      return series
        .SlidingWindow(2)
        .Scan(
          new double[] { 0, 0 }, // dummy
          (state, s) => new double[]
          {
            s[1][0],
            (s[1][1]-s[0][1])/(s[1][0]-s[0][0])
          }
        );
      //.Skip(1);
    }

    /// <summary>
    /// integrates Y values while assuming monotonically increasing X's
    /// </summary>
    /// <param name="series"></param>
    /// <returns>integrated values of Y for each consequtive X value</returns>
    /// <remarks>f(x2)(x2-x1)-1/2(f(x2)-f(x1))(x2-x1)</remarks>
    public static IEnumerable<double> Integrate(this IEnumerable<double> series)
    {
      Contract.Requires<ArgumentNullException>(series != null);
      Contract.Ensures(Contract.Result<IEnumerable<double>>().Count() == series.Count());
      Contract.Ensures(Contract.ForAll(Contract.Result<IEnumerable<double>>().SlidingWindow(2), o => o[0] <= o[1]));

      return series
        .Select((y, i) => new double[] { i, y })
        .Integrate()
        .Select(s => s[1]);
    }

    /// <summary>
    /// Integrates pairs of {decimal,decimal} where decimal[0] is X and decimal[1] is Y
    /// </summary>
    /// <param name="series">array of {decimal,decimal}</param>
    /// <returns>integrated values of Y for each consequtive X value</returns>
    /// <remarks>f(x2)(x2-x1)-1/2(f(x2)-f(x1))(x2-x1)</remarks>
    public static IEnumerable<double[]> Integrate(this IEnumerable<double[]> series)
    {
      Contract.Requires<ArgumentNullException>(series != null);
      Contract.Requires<ArgumentException>(Contract.ForAll(series, s => s != null));
      Contract.Requires<ArgumentException>(Contract.ForAll(series, s => s.Length >= 2));
      Contract.Ensures(Contract.Result<IEnumerable<double[]>>().Count() == series.Count());
      Contract.Ensures(Contract.ForAll(Contract.Result<IEnumerable<double[]>>(), o => o.Count() == 2));
      Contract.Ensures(Contract.ForAll(Contract.Result<IEnumerable<double[]>>().SlidingWindow(2), o => o[0][1] <= o[1][1]));
      Contract.Ensures(Contract.ForAll(Contract.Result<IEnumerable<double[]>>().Zip(series, (o, s) => new { O = o[0], S = s[0] }), z => z.O == z.S));

      Contract.Assume(series.First().Length > 0);
      var initialState = new double[] { series.First()[0], 0 };
      return series
        .SlidingWindow(2)
        .Scan0(initialState, (state, s) =>
          //.Scan((state, s) =>
          new double[]
          { s[1][0],
            s[1][1]*(s[1][0]-s[0][0]) - ((0.5*(s[1][1]- s[0][1]))*(s[1][0]-s[0][0]))
          });
    }

    /// <summary>
    /// Normalizes series using first element as base value
    /// </summary>
    /// <param name="series"></param>
    /// <returns></returns>
    public static IEnumerable<double> Normalize(IEnumerable<double> series)
    {
      Contract.Requires<ArgumentNullException>(series != null);
      Contract.Ensures(Contract.Result<IEnumerable<double>>().Count() == series.Count());

      return Normalize(series, series.First());
    }

    public static IEnumerable<double> Normalize(IEnumerable<double> series, double baseValue)
    {
      return series
          .Select(x => x / baseValue);
    }

    /// <summary>
    /// Normalizes series using first element as base value
    /// </summary>
    /// <param name="series"></param>
    /// <returns></returns>
    public static IEnumerable<decimal> Normalize(this IEnumerable<decimal> series)
    {
      Contract.Requires<ArgumentNullException>(series != null);
      Contract.Ensures(Contract.Result<IEnumerable<decimal>>().Count() == series.Count());

      return Normalize(series, series.First());
    }

    public static IEnumerable<decimal> Normalize(this IEnumerable<decimal> series, decimal baseValue)
    {
      Contract.Requires<ArgumentNullException>(series != null);
      Contract.Ensures(Contract.Result<IEnumerable<decimal>>().Count() == series.Count());

      return series
          .Select(x => x / baseValue);
    }

    public static IEnumerable<decimal> PercentageChange(this IEnumerable<decimal> series, int decimalPlaces = 28)
    {
      Contract.Requires<ArgumentNullException>(series != null);
      Contract.Requires<ArgumentException>(series.Count() >= 2);
      Contract.Ensures(Contract.Result<IEnumerable<decimal>>().Count() == series.Count() - 1);

      return series
        .SlidingWindow(2)
        .Select(x => Math.Round(x[1] / x[0] - 1, decimalPlaces));
    }

    public static IEnumerable<TResult> PercentageChange<TResult>(this IEnumerable<TResult> series)
    {
      Contract.Requires<ArgumentNullException>(series != null);
      Contract.Requires<ArgumentException>(series.Count() >= 2);
      Contract.Ensures(Contract.Result<IEnumerable<TResult>>().Count() == series.Count() - 1);

      return series.PercentageChange(x => x);
    }

    public static IEnumerable<TResult> PercentageChange<T, TResult>(this IEnumerable<T> series, Func<T, TResult> selector)
    {
      Contract.Requires<ArgumentNullException>(series != null);
      Contract.Requires<ArgumentNullException>(selector != null);
      Contract.Requires<ArgumentException>(series.Count() >= 2);
      Contract.Ensures(Contract.Result<IEnumerable<TResult>>().Count() == series.Count() - 1);

      return series
        .Select(x => selector(x))
        .SlidingWindow(2)
        .Select(x => Operator.Divide(Operator.Subtract(x[1], x[0]), x[0]));
    }

    public static IEnumerable<double> ReversePercentageChange(IEnumerable<double> series, double baseValue = 1, bool includeBaseValueInResult = true)
    {
      Contract.Requires<ArgumentNullException>(series != null);
      Contract.Ensures(Contract.Result<IEnumerable<double>>().Count() == series.Count() + (includeBaseValueInResult ? 1 : 0));


      Func<IEnumerable<double>, double, Func<double, double, double>, IEnumerable<double>> resultFunc;
      if (includeBaseValueInResult)
      {
        resultFunc = LinqExtensions.Scan0;
      }
      else
      {
        resultFunc = EnumerableEx.Scan;
      }

      var result = resultFunc
          (
            series,
            baseValue,
            (aggregate, x) => aggregate * (1 + x)
          );

      return result;
    }

    public static IEnumerable<decimal> ReversePercentageChange(this IEnumerable<decimal> series, decimal baseValue = 1, int decimalPlaces = 28)
    {
      Contract.Requires<ArgumentNullException>(series != null);
      Contract.Ensures(Contract.Result<IEnumerable<decimal>>().Count() == series.Count());

      return series
          .Scan(
              baseValue,
              (aggregate, x) => Math.Round(aggregate * (1 + x), decimalPlaces)
          );
    }
  }
}