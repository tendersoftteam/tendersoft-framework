﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Diagnostics.Contracts;

namespace Tendersoft.Calculation
{
  public class ShanonEntropy<T>
  {
    private IDictionary<T, int> tokenFrequencies;
    private IList<T> set;
    private bool maxEntropyDetermined = false;

    public ShanonEntropy(IEnumerable<T> set)
    {
      Contract.Requires(set != null);

      this.set = set.ToList();
      this.tokenFrequencies = GetTokenFrequencies(set);

      var setType = this.set.GetType();
      if (setType != null)
      {
        var elementType = setType.GetElementType();
        if (elementType != null)
        {
          var elementBitSize = System.Runtime.InteropServices.Marshal.SizeOf(elementType) * 8;
          //var maxTokens = Math.Pow(2, elementBitSize);
          this.MaxEntropy = elementBitSize;
          this.maxEntropyDetermined = true;
        }
      }
    }

    public int TokenCount
    {
      get { return this.tokenFrequencies.Count; }
    }

    public double MaxEntropy
    {
      get { throw new NotImplementedException("get property"); }
      private set { throw new NotImplementedException("set property"); }
    }

    public double EntropyRatio
    {
      get { throw new NotImplementedException("get property"); }
      private set { throw new NotImplementedException("set property"); }
    }

    private static IDictionary<T, int> GetTokenFrequencies(IEnumerable<T> set)
    {
      var tokenFreqs = set.GroupBy(x => x)
          .ToDictionary(x => x.Key, x => x.Count());
      return tokenFreqs;
    }

    /// <summary>
    /// Return entropy (in bits) of this buffer of bytes. We assume that the
    /// `length' parameter is correct. This implementation is a translation
    /// of the PHP code found here:
    ///
    ///    http://onlamp.com/pub/a/php/2005/01/06/entropy.html
    ///
    /// with a helpful hint on the `foreach' statement from here:
    ///
    ///    http://php.net/manual/en/control-structures.foreach.php
    /// </summary>
    /// <returns>Shannon entropy</returns>
    public double CalculateEntropyH()
    {
      var entropy = 0.0;
      //var cumulativeError=0.0;
      foreach (var frequency in this.tokenFrequencies.Values)
      {
        var tokenProbability = (double)frequency / (double)this.set.Count;
        var log2 = Math.Log(tokenProbability, 2);
        //var pow2 = Math.Pow(2, log2);
        //var log2err = tokenProbability - pow2;
        //var errorCorrection = log2err * tokenProbability;
        //cumulativeError += errorCorrection;
        ////var partialEntropy = tokenProbability * (log2+log2err);
        var partialEntropy_ = tokenProbability * log2;
        ////var partialError = partialEntropy - partialEntropy_;
        ////var s = partialError;
        entropy += partialEntropy_;
      }
      //entropy += cumulativeError;

      var bits = -entropy;
      if (!this.maxEntropyDetermined)
      {
        this.MaxEntropy = (int)Math.Log(this.TokenCount, 2);
      }
      if (bits - this.MaxEntropy > double.Epsilon)
      {
        // calculation error due to rounding
        bits = this.MaxEntropy;
        this.EntropyRatio = 1;
      }
      else
      {
        this.EntropyRatio = bits / this.MaxEntropy;
      }

      return bits;
    }
  }

  //// Notice: non-generic class overload. Possible in C#! <-trolling or just stupid? O_o
  //public class ShanonEntropy
  //{
  //    public static ShanonEntropy<T> Create<T>(IEnumerable<T> x) { return new ShanonEntropy<T>(x); }
  //}
}