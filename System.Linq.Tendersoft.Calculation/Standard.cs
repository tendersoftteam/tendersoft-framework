using System;

namespace Tendersoft.Calculation
{
  public static class Standard
  {
    public static int Max(int[] srcTab)
    {
      int max = int.MinValue;
      int lenght = srcTab.Length;
      for (int i = 0; i < srcTab.Length; i++)
      {
        if (max < srcTab[i])
        {
          max = srcTab[i];
        }
      }
      return max;
    }

    public static float Max(float[] srcTab)
    {
      float max = float.MinValue;
      int lenght = srcTab.Length;
      for (int i = 0; i < srcTab.Length; i++)
      {
        if (max < srcTab[i])
        {
          max = srcTab[i];
        }
      }
      return max;
    }

    public static int Max(int[] srcTab, out int maxIndex)
    {
      int max = int.MinValue;
      int lenght = srcTab.Length;
      maxIndex = 0;
      for (int i = 0; i < srcTab.Length; i++)
      {
        if (max < srcTab[i])
        {
          max = srcTab[i];
          maxIndex = i;
        }
      }
      return max;
    }

    public static float Max(float[] srcTab, out int maxIndex)
    {
      float max = float.MinValue;
      int lenght = srcTab.Length;
      maxIndex = 0;
      for (int i = 0; i < srcTab.Length; i++)
      {
        if (max < srcTab[i])
        {
          max = srcTab[i];
          maxIndex = i;
        }
      }
      return max;
    }

    public static int MaxIndex(int[] srcTab)
    {
      int max = int.MinValue;
      int index = 0;
      int lenght = srcTab.Length;
      for (int i = 0; i < srcTab.Length; i++)
      {
        if (max < srcTab[i])
        {
          max = srcTab[i];
          index = i;
        }
      }
      return index;
    }

    public static float MaxIndex(float[] srcTab)
    {
      float max = float.MinValue;
      int index = 0;
      int lenght = srcTab.Length;
      for (int i = 0; i < srcTab.Length; i++)
      {
        if (max < srcTab[i])
        {
          max = srcTab[i];
          index = i;
        }
      }
      return index;
    }

    public static float ConvertToDegrees(decimal value)
    {
      return (float)(Convert.ToDouble(value) * 180.0 / Math.PI);
    }

    public static float ConvertToDegrees(double value)
    {
      return (float)(value * 180.0 / Math.PI);
    }

    public static float ConvertToRadians(decimal value)
    {
      return (float)(Convert.ToDouble(value) * Math.PI / 180.0);
    }

    public static float ConvertToRadians(double value)
    {
      return (float)(value * Math.PI / 180.0);
    }
  }
}