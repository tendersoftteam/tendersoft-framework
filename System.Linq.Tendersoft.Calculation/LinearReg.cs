using System;
using System.Collections.Generic;
using System.Drawing;

namespace Tendersoft.Calculation
{
  public class LinearReg
  {
    private List<PointF> pointPairList;
    public List<PointF> PointPairList
    {
      get
      {
        return pointPairList;
      }
      set
      {
        pointPairList = value;
        CalculateLinearRegression();
      }
    }

    public LinearReg()
    {
      pointPairList = new List<PointF>();
    }

    /// <summary>
    /// Initializes the class by calculating the slope, intercept and
    ///	regression coefficient based on the given constructor arguments.
    /// note: The slope should not be infinite.
    /// </summary>
    /// <param name="n"></param>
    /// <param name="x"></param>
    /// <param name="y"></param>
    public LinearReg(List<PointF> data)
    {
      this.pointPairList = data;
      CalculateLinearRegression();
    }

    private void CalculateLinearRegression()
    {
      // calculate the averages of arrays x and y
      double xa = 0, ya = 0;
      for (int i = 0; i < this.pointPairList.Count; i++)
      {
        xa += this.pointPairList[i].X;
        ya += this.pointPairList[i].Y;
      }
      xa /= this.pointPairList.Count;
      ya /= this.pointPairList.Count;

      // calculate auxiliary sums
      double xx = 0, yy = 0, xy = 0;
      for (int i = 0; i < this.pointPairList.Count; i++)
      {
        double tmpx = this.pointPairList[i].X - xa, tmpy = this.pointPairList[i].Y - ya;
        xx += tmpx * tmpx;
        yy += tmpy * tmpy;
        xy += tmpx * tmpy;
      }

      // calculate regression line parameters

      // make sure slope is not infinite
      // instead of: if (Math.Abs(xx) != 0.0) use:
      if (Math.Abs(xx) > double.Epsilon)
      {
        m_b = xy / xx;
        m_a = ya - m_b * xa;
        // instead of: m_coeff = (Math.Abs(yy) == 0) ? 1 : xy / Math.Sqrt(xx * yy); use:
        m_coeff = (Math.Abs(yy) < double.Epsilon) ? 1 : xy / Math.Sqrt(xx * yy);
      }
      else
      {
        m_b = 0;
        m_a = ya;
        m_coeff = 1; //???
      }
    }

    private double m_a, m_b, m_coeff;

    /// <summary>
    /// Evaluates the linear regression function at the given abscissa.
    /// </summary>
    /// <param name="x">the abscissa used to evaluate the linear regression function</param>
    /// <returns></returns>
    public double getValue(double x)
    {
      return m_a + m_b * x;
    }

    /// <summary>
    /// Returns the slope of the regression line
    /// </summary>
    public double getSlope
    {
      get { return m_b; }
    }

    /// <summary>
    /// Returns the intercept on the Y axis of the regression line
    /// </summary>
    /// <returns></returns>
    public double getIntercept
    {
      get { return m_a; }
    }

    /// <summary>
    /// Returns the linear regression coefficient
    ///
    ///The regression coefficient indicated how well linear regression fits to the
    ///original data. It is an expression of error in the fitting and is defined as:
    ///\f[ r = \frac{S_{xy}}{\sqrt{S_{x} \cdot S_{y}}} \f]
    ///
    ///This varies from 0 (no linear trend) to 1 (perfect linear fit). If \f$ |S_y| =
    ///0\f$ and \f$ |S_x| \neq 0 \f$,   then \e r is considered to be equal to 1.
    /// </summary>
    public double getCoefficient
    {
      get { return m_coeff; }
    }

    public static double CalculateSlope(Point p1, Point p2)
    {
      // F[a;b] straight line between 2 points
      // slope: m=(y2-y1)/(x2-x1)
      var slope = (p2.Y - p1.Y) / (p2.X - p1.X);

      return slope;
    }

    public static double CalculateIntercept(Point p1, double slope)
    {
      // intercept: b = y1 - m*x1 = y2 - m*x1
      var intercept0 = p1.Y - slope * p1.X;

      return intercept0;
    }

    public static Tuple<double, double> CalculateSlopeIntercept(Point p1, Point p2)
    {
      var slope = CalculateSlope(p1, p2);
      var intercept = CalculateIntercept(p1, slope);
      return Tuple.Create(slope, intercept);
    }

    public static double CalculateSlope(Tuple<double, double> p1, Tuple<double, double> p2)
    {
      return CalculateSlope(p1.Item2, p2.Item2, p1.Item1, p2.Item1);
    }

    public static double CalculateSlope(double y1, double y2, double x1, double x2)
    {
      // F[a;b] straight line between 2 points
      // slope: m=(y2-y1)/(x2-x1)
      var slope = (y2 - y1) / (x2 - x1);

      return slope;
    }

    public static double CalculateIntercept(Tuple<double, double> p, double slope)
    {
      return CalculateIntercept(p.Item2, p.Item1, slope);
    }

    public static double CalculateIntercept(double y, double x, double slope)
    {
      // intercept: b = y1 - m*x1 = y2 - m*x1
      var intercept0 = y - slope * x;

      return intercept0;
    }

    /// <summary>
    /// Calculates slope and intercept given two points
    /// </summary>
    /// <param name="p1">point 1:[x,y]</param>
    /// <param name="p2">point 2:[x,y]</param>
    /// <returns>Tuple(Slope,Intercept)</returns>
    public static Tuple<double, double> CalculateSlopeIntercept(Tuple<double, double> p1, Tuple<double, double> p2)
    {
      return CalculateSlopeIntercept(p1.Item2, p2.Item2, p1.Item1, p2.Item1);
    }

    /// <summary>
    /// Calculates slope and intercept given x,y values of two points
    /// </summary>
    /// <param name="y1"></param>
    /// <param name="y2"></param>
    /// <param name="x1"></param>
    /// <param name="x2"></param>
    /// <returns>Tuple(Slope,Intercept)</returns>
    public static Tuple<double, double> CalculateSlopeIntercept(double y1, double y2, double x1, double x2)
    {
      var slope = CalculateSlope(y1, y2, x1, x2);
      var intercept = CalculateIntercept(y1, x1, slope);

      return Tuple.Create(slope, intercept);
    }
  }
}