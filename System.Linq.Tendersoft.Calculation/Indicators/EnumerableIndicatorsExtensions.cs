﻿using System.Collections.Generic;
using System.Diagnostics.Contracts;
using TicTacTec.TA.Library;

namespace System.Linq.Tendersoft.Calculation.Indicators
{
  public static class EnumerableIndicatorsExtensions
  {
    /// <summary>
    /// Calculates BolingerBands
    /// </summary>
    /// <param name="source">input data</param>
    /// <param name="period">defaults to 20</param>
    /// <param name="deviation">defaults to 2</param>
    /// <returns></returns>
    public static IEnumerable<double[]> BolingerBands(this IEnumerable<double> source, int period = 20, double deviation = 2.0)
    {
      Contract.Requires<ArgumentNullException>(source != null);
      Contract.Requires<ArgumentOutOfRangeException>(source.Count() > period);
      Contract.Ensures(
        Contract.ForAll(
          Contract.Result<IEnumerable<double[]>>().Skip(period - 1),
          x => (x[0] >= x[1]) && (x[1] >= x[2])
        )
      );

      return source.BolingerBands(period, deviation, deviation);
    }

    /// <summary>
    /// Calculates BolingerBands with different top/bottom band
    /// </summary>
    /// <param name="source">input data</param>
    /// <param name="period">defaults to 20</param>
    /// <param name="upperDev">defaults to 2</param>
    /// <param name="lowerDev">defaults to 2</param>
    /// <returns></returns>
    public static IEnumerable<double[]> BolingerBands(this IEnumerable<double> source, int period = 20, double upperDev = 2.0, double lowerDev = 2.0)
    {
      Contract.Requires<ArgumentNullException>(source != null);
      Contract.Requires<ArgumentOutOfRangeException>(source.Count() > period);
      Contract.Ensures(
        Contract.ForAll(
          Contract.Result<IEnumerable<double[]>>().Skip(period - 1),
          x => (x[0] >= x[1]) && (x[1] >= x[2])
        )
      );
      Contract.Ensures(Contract.ForAll(Contract.Result<IEnumerable<double[]>>(), x => x.Length == 3));

      var inSource = source.ToArray();
      var startIndex = 0;
      var endIndex = inSource.Length - 1;

      var lookback = Core.BbandsLookback(period, upperDev, lowerDev, Core.MAType.Sma);
      var temp = Math.Max(lookback, startIndex);
      var allocationSize = CalculateAllocationSize(endIndex, temp);
      int outBeginIndex, outNumberOfElements;

      double[] lowerBound = new double[allocationSize];
      double[] middleBound = new double[allocationSize];
      double[] upperBound = new double[allocationSize];

      var result = Core.Bbands(
          startIndex, endIndex, inSource,
          period, upperDev, lowerDev, Core.MAType.Sma,
          out outBeginIndex, out outNumberOfElements,
          upperBound, middleBound, lowerBound);

      if (result == Core.RetCode.Success)
      {
        for (int i = 0; i < allocationSize; i++)
        {
          yield return new double[] { upperBound[i], middleBound[i], lowerBound[i] };
        }
      }
      else
      {
        throw new Exception(Enum.GetName(result.GetType(), result));
      }
    }

    public static IEnumerable<double> Dema2(this IEnumerable<double> source, int period = 13)
    {
      Contract.Requires<ArgumentNullException>(source != null);
      Contract.Requires<ArgumentOutOfRangeException>(period > 0, "DEMA period must be more than 0");
      Contract.Requires<ArgumentOutOfRangeException>(source.Count() >= period);
      //Contract.Requires<ArgumentOutOfRangeException>(autodetectSmoothingFactor && alphaDecimalPrecision > 0);
      //Contract.Requires<ArgumentOutOfRangeException>(autodetectSmoothingFactor && alphaPrecisionBase > 1);

      //if (!autodetectSmoothingFactor)
      //{
      var alpha = 2 / (double)(1 + period);
      throw new NotImplementedException("Dummy based on Statistic.CalculateEma(...) function");
      // return current EMA
      var emas = source
        .Scan((prevEma, x) => Statistic.CalculateEma(x, prevEma, alpha));

      return emas;
    }

    public static IEnumerable<double> Ema(this IEnumerable<double> source, int period = 13, bool padPrePeriodResults = false, double defaultPadValue = 0, int forecastSteps = 0)
    {
      Contract.Requires<ArgumentNullException>(source != null);
      Contract.Requires<ArgumentOutOfRangeException>(period > 0, "EMA period must be more than 0");
      Contract.Requires<ArgumentOutOfRangeException>(source.Count() >= period);
      Contract.Ensures(
        (padPrePeriodResults && Contract.Result<IEnumerable<double>>().Count() - forecastSteps == source.Count())
      ^ (!padPrePeriodResults && Contract.Result<IEnumerable<double>>().Count() - forecastSteps < source.Count()));
      //Contract.Ensures(Contract.ForAll(Contract.Result<IEnumerable<double?>>().Take(period - 1), x => !x.HasValue));
      //Contract.Ensures(Contract.ForAll(Contract.Result<IEnumerable<double?>>().Skip(period - 1), x => x.HasValue));

      var inSource = source.ToArray();
      var startIndex = 0;
      var endIndex = inSource.Length - 1;
      int outBegIdx, outNBElement;

      var lookback = Core.EmaLookback(period);
      var temp = Math.Max(lookback, startIndex);
      var allocationSize = CalculateAllocationSize(endIndex, temp);

      var outReal = new double[allocationSize];

      var result = Core.Ema(startIndex, endIndex, inSource, period, out outBegIdx, out outNBElement, outReal);
      if (result == Core.RetCode.Success)
      {
        if (padPrePeriodResults)
        {
          var alpha = 2 / (double)(1 + period);
          var forecast = Enumerable.Range(0, forecastSteps)
            .Scan(
              Statistic.CalculateEma(inSource.Last(), outReal.Last(), alpha),
              (state, n) => Statistic.CalculateEma(inSource.Last(), state, alpha)
            )
            .Take(forecastSteps);

          return Enumerable.Repeat(defaultPadValue, period - 1)
            .Concat(outReal)
            .Concat(forecast);
        }
        return outReal;//.Select((x, n) => n < outBegIdx ? (double?)null : x);
      }
      else
      {
        throw new Exception(Enum.GetName(result.GetType(), result));
      }
    }

    public static IEnumerable<double> Ema2(this IEnumerable<double> source, int period = 13, int forecastSteps = 0)//, bool autodetectSmoothingFactor = false, int alphaDecimalPrecision = 3, int alphaPrecisionBase = 10)
    {
      Contract.Requires<ArgumentNullException>(source != null);
      Contract.Requires<ArgumentOutOfRangeException>(period > 0, "EMA period must be more than 0");
      Contract.Requires<ArgumentOutOfRangeException>(source.Count() >= period);
      Contract.Ensures(Contract.Result<IEnumerable<double>>().Count() - forecastSteps == source.Count());
      //Contract.Requires<ArgumentOutOfRangeException>(autodetectSmoothingFactor && alphaDecimalPrecision > 0);
      //Contract.Requires<ArgumentOutOfRangeException>(autodetectSmoothingFactor && alphaPrecisionBase > 1);

      //if (!autodetectSmoothingFactor)
      //{
      var alpha = 2 / (double)(1 + period);
      // return current EMA
      var emas = source
        .Scan(
          source.First(),
          (prevEma, x) => Statistic.CalculateEma(x, prevEma, alpha)
        );
      Contract.Assert(emas.Count() == source.Count());

      var lastEma = 0.0;
      foreach (var currentEma in emas)
      {
        lastEma = currentEma;
        yield return currentEma;
      }
      for (int i = 0; i < forecastSteps; i++)
      {
        lastEma = Statistic.CalculateEma(source.Last(), lastEma, alpha);
        yield return lastEma;
      }
    }

    /// <summary>
    /// MACD
    /// </summary>
    /// <param name="source"></param>
    /// <param name="fastPeriod">default is 12</param>
    /// <param name="slowPeriod">default is 26</param>
    /// <param name="signalPeriod">default is 9</param>
    /// <returns>sequence of double[]{MACD, MACDSignal, MACDHist} values</returns>
    public static IEnumerable<double[]> Macd(this IEnumerable<double> source, int fastPeriod = 12, int slowPeriod = 26, int signalPeriod = 9)
    {
      Contract.Requires<ArgumentNullException>(source != null);
      Contract.Requires<ArgumentOutOfRangeException>(source.Count() > slowPeriod);
      Contract.Ensures(Contract.ForAll(Contract.Result<IEnumerable<double[]>>(), x => x.Length == 3));

      var inSource = source.ToArray();
      var startIndex = 0;
      var endIndex = inSource.Length - 1;
      int outBeginIndex, outNumberOfElements;

      var lookback = Core.MacdLookback(fastPeriod, slowPeriod, signalPeriod);
      var temp = Math.Max(lookback, startIndex);
      var allocationSize = CalculateAllocationSize(endIndex, temp);

      var outMACD = new double[allocationSize];
      var outMACDSignal = new double[allocationSize];
      var outMACDHist = new double[allocationSize];

      var result = Core.Macd(
        startIndex, endIndex, inSource,
        fastPeriod, slowPeriod, signalPeriod,
        out outBeginIndex, out outNumberOfElements,
        outMACD, outMACDSignal, outMACDHist);

      if (result == Core.RetCode.Success)
      {
        for (int i = 0; i < allocationSize; i++)
        {
          yield return new double[] { outMACD[i], outMACDSignal[i], outMACDHist[i] };
        }
      }
      else
      {
        throw new Exception(Enum.GetName(result.GetType(), result));
      }
    }

    public static double[] Rsi(this IEnumerable<double> source, int period = 14)
    {
      Contract.Requires<ArgumentNullException>(source != null);
      Contract.Requires<ArgumentOutOfRangeException>(period > 0, "RSI period must be more than 0");
      Contract.Requires<ArgumentOutOfRangeException>(source.Count() >= period);
      Contract.Ensures(Contract.Result<double[]>().Count() <= source.Count() - period);
      Contract.Ensures(Contract.ForAll(Contract.Result<double[]>(), x => x >= 0));
      Contract.Ensures(Contract.ForAll(Contract.Result<double[]>(), x => x <= 100));

      var inSource = source.ToArray();
      var startIndex = 0;
      var endIndex = inSource.Length - 1;
      int outBegIdx, outNBElement;

      var lookback = Core.RsiLookback(period);
      var temp = Math.Max(lookback, startIndex);
      var allocationSize = CalculateAllocationSize(endIndex, temp);

      var outReal = new double[allocationSize];

      var result = Core.Rsi(startIndex, endIndex, inSource, period, out outBegIdx, out outNBElement, outReal);
      if (result == Core.RetCode.Success)
      {
        return outReal;
      }
      else
      {
        throw new Exception(Enum.GetName(result.GetType(), result));
      }
    }

    /// <summary>
    /// Calculates SlowStochastic: {low, high, close} -> {SlowK, SlowD}
    /// </summary>
    /// <param name="source">input data</param>
    /// <param name="fastKPeriod">defaults to 14</param>
    /// <param name="slowKPeriod">defaults to 21</param>
    /// <param name="slowDPeriod">defaults to 3</param>
    /// <param name="trimInvalidValuesAtBegining">removes from the output those starting values that are not yet valid, default is false</param>
    /// <returns>double[2][]{SlowK sequence, SlowD sequence} values</returns>
    public static double[][] SlowStachastic(this IEnumerable<Tuple<double, double, double>> source, int fastKPeriod = 14, int slowKPeriod = 21, int slowDPeriod = 3, bool trimInvalidValuesAtBegining = false)
    {
      Contract.Requires<ArgumentNullException>(source != null);
      Contract.Requires<ArgumentOutOfRangeException>(fastKPeriod > 0, "SlowStochastic fastKPeriod must be more than 0");
      Contract.Requires<ArgumentOutOfRangeException>(slowKPeriod > 0, "Stochastic RSI slowKPeriod must be more than 0");
      Contract.Requires<ArgumentOutOfRangeException>(slowDPeriod > 0, "Stochastic RSI slowDPeriod must be more than 0");
      Contract.Requires<ArgumentOutOfRangeException>(source.Count() > Math.Max(Math.Max(fastKPeriod, slowKPeriod), slowDPeriod));
      Contract.Ensures(Contract.Result<double[][]>().Length == 2);
      Contract.Ensures(Contract.ForAll(Contract.Result<double[][]>(), s => s.Count() <= source.Count() - Math.Max(Math.Max(fastKPeriod, slowKPeriod), slowDPeriod)));
      Contract.Ensures(Contract.ForAll(Contract.Result<double[][]>().Select(x => x[0]).Skip(trimInvalidValuesAtBegining ? 0 : fastKPeriod - 1), x => x >= 0));
      Contract.Ensures(Contract.ForAll(Contract.Result<double[][]>().Select(x => x[0]).Skip(trimInvalidValuesAtBegining ? 0 : fastKPeriod - 1), x => x <= 100));
      Contract.Ensures(Contract.ForAll(Contract.Result<double[][]>().Select(x => x[1]).Skip(trimInvalidValuesAtBegining ? 0 : fastKPeriod - 1), x => x >= 0));
      Contract.Ensures(Contract.ForAll(Contract.Result<double[][]>().Select(x => x[1]).Skip(trimInvalidValuesAtBegining ? 0 : fastKPeriod - 1), x => x <= 100));

      var lowSeries = source.Select(x => x.Item1).ToArray();
      var highSeries = source.Select(x => x.Item2).ToArray();
      var closeSeries = source.Select(x => x.Item3).ToArray();

      var startIndex = 0;
      var endIndex = closeSeries.Length - 1;
      int outBegIdx, outNBElement;

      var lookback = Core.StochLookback(fastKPeriod, slowKPeriod, Core.MAType.Ema, slowDPeriod, Core.MAType.Ema);
      var temp = Math.Max(lookback, startIndex);
      var allocationSize = CalculateAllocationSize(endIndex, temp);

      var outSlowK = new double[allocationSize];
      var outSlowD = new double[allocationSize];

      var result = TicTacTec.TA.Library.Core.Stoch(
          startIndex, endIndex, // input
          highSeries, lowSeries, closeSeries, //input
          fastKPeriod, slowKPeriod, Core.MAType.Sma, //options
          slowDPeriod, Core.MAType.Sma, //options
          out outBegIdx, out outNBElement, //output
          outSlowK, outSlowD); //output

      if (result == Core.RetCode.Success)
      {
        Contract.Assert(outSlowK.Length == outSlowD.Length);
        var outSlowKs = outSlowK.AsEnumerable();
        var outSlowDs = outSlowD.AsEnumerable();
        if (trimInvalidValuesAtBegining)
        {
          outSlowKs = outSlowKs.Skip(outBegIdx);
          outSlowDs = outSlowDs.Skip(outBegIdx);
        }

        var output = new double[][]
        {
          outSlowKs.ToArray(),
          outSlowDs.ToArray()
        };

        return output;
      }
      else
      {
        throw new Exception(Enum.GetName(result.GetType(), result));
      }
    }

    public static IEnumerable<double?> Sma2(this IEnumerable<double> source, int period = 13)
    {
      Contract.Requires<ArgumentNullException>(source != null);
      Contract.Requires<ArgumentOutOfRangeException>(period > 0, "SMA period must be more than 0");
      Contract.Requires<ArgumentOutOfRangeException>(source.Count() >= period);
      Contract.Ensures(Contract.ForAll(Contract.Result<IEnumerable<double?>>().Take(period - 1), x => !x.HasValue));
      Contract.Ensures(Contract.ForAll(Contract.Result<IEnumerable<double?>>().Skip(period - 1), x => x.HasValue));

      var averageList = source.Take(period - 1).ToList();
      foreach (var item in averageList)
      {
        yield return null;
      }
      if (averageList.Count < period - 1)
      {
        yield break;
      }

      var average = averageList.Average();
      yield return average;
      var sum = averageList.Sum();

      foreach (var item in source.Skip(period))
      {
        sum -= averageList[0];
        sum += item;
        average = sum / period;
        yield return average;
        // update list
        averageList.RemoveAt(0);
        averageList.Add(item);
      }
    }

    /// <summary>
    /// Stochastic RSI function
    /// http://www.tadoc.org/forum/index.php?topic=436.0
    /// </summary>
    /// <param name="source">input data</param>
    /// <param name="period">defaults to 14</param>
    /// <param name="fastKPeriod">Unsmoothed RSI, defaults to 14</param>
    /// <param name="fastDPeriod">Smoothed RSI, defaults to 14</param>
    /// <param name="trimInvalidValuesAtBegining">removes from the output those starting values that are not yet valid, default is false</param>
    /// <returns>int[2][]{fastK sequence, fastD sequence}</returns>
    /// <remarks>Note EMA is an unstable function i.e. first few elements are inpacted by previous values</remarks>
    public static int[][] StochasticRsi(this IEnumerable<double> source, int period = 14, int fastKPeriod = 14, int fastDPeriod = 14, bool trimInvalidValuesAtBegining = false)
    {
      Contract.Requires<ArgumentNullException>(source != null);
      Contract.Requires<ArgumentOutOfRangeException>(period > 0, "Stochastic RSI period must be more than 0");
      Contract.Requires<ArgumentOutOfRangeException>(fastKPeriod > 0, "Stochastic RSI fastKPeriod must be more than 0");
      Contract.Requires<ArgumentOutOfRangeException>(fastDPeriod > 0, "Stochastic RSI fastDPeriod must be more than 0");
      Contract.Requires<ArgumentOutOfRangeException>(source.Count() >= Math.Max(Math.Max(period, fastKPeriod), fastDPeriod));
      Contract.Ensures(Contract.Result<int[][]>().Length == 2);
      Contract.Ensures(Contract.ForAll(Contract.Result<int[][]>(), s=>s.Count() <= source.Count() - Math.Max(Math.Max(period, fastKPeriod), fastDPeriod)));
      Contract.Ensures(Contract.ForAll(Contract.Result<int[][]>().Select(x => x[0]).Skip(trimInvalidValuesAtBegining ? 0 : period - 1), x => x >= 0));
      Contract.Ensures(Contract.ForAll(Contract.Result<int[][]>().Select(x => x[0]).Skip(trimInvalidValuesAtBegining ? 0 : period - 1), x => x <= 100));
      Contract.Ensures(Contract.ForAll(Contract.Result<int[][]>().Select(x => x[1]).Skip(trimInvalidValuesAtBegining ? 0 : period - 1), x => x >= 0));
      Contract.Ensures(Contract.ForAll(Contract.Result<int[][]>().Select(x => x[1]).Skip(trimInvalidValuesAtBegining ? 0 : period - 1), x => x <= 100));

      var inSource = source.ToArray();
      var startIndex = 0;
      var endIndex = inSource.Length - 1;
      int outBegIdx, outNBElement;

      var lookback = Core.StochRsiLookback(period, fastKPeriod, fastDPeriod, Core.MAType.Ema);
      var temp = Math.Max(lookback, startIndex);
      var allocationSize = CalculateAllocationSize(endIndex, temp);

      var outFastK = new double[allocationSize];
      var outFastD = new double[allocationSize];

      var result = Core.StochRsi(
        startIndex, endIndex, inSource, // input
        period, fastKPeriod, fastDPeriod, Core.MAType.Ema, // options
        out outBegIdx, out outNBElement, outFastK, outFastD); // output

      if (result == Core.RetCode.Success)
      {
        Contract.Assert(outFastK.Length == outFastD.Length);
        var outFastKInts = outFastK.Select(x => (int)x);
        var outFastDInts = outFastD.Select(x => (int)x);
        if (trimInvalidValuesAtBegining)
        {
          outFastKInts = outFastKInts.Skip(outBegIdx);
          outFastDInts = outFastDInts.Skip(outBegIdx);
        }

        var output = new int[][]
        {
          outFastKInts.ToArray(),
          outFastDInts.ToArray()
        };

        return output;
      }
      else
      {
        throw new Exception(Enum.GetName(result.GetType(), result));
      }
    }

    public static IEnumerable<double> Tema(this IEnumerable<double> source, int period = 21, bool padPrePeriodResults = false, double defaultPadValue = 0)
    {
      Contract.Requires<ArgumentNullException>(source != null);
      Contract.Requires<ArgumentOutOfRangeException>(period > 0, "EMA period must be more than 0");
      Contract.Requires<ArgumentOutOfRangeException>(source.Count() >= period);
      Contract.Ensures(
        (padPrePeriodResults && Contract.Result<IEnumerable<double>>().Count() == source.Count())
      ^ (!padPrePeriodResults && Contract.Result<IEnumerable<double>>().Count() < source.Count()));

      var inSource = source.ToArray();
      var startIndex = 0;
      var endIndex = inSource.Length - 1;
      int outBeginIndex, outNumberOfElements;

      var lookback = Core.TemaLookback(period);
      var temp = Math.Max(lookback, startIndex);
      var allocationSize = CalculateAllocationSize(endIndex, temp);

      var output = new double[allocationSize];

      var result = Core.Tema(startIndex, endIndex, inSource, period, out outBeginIndex, out outNumberOfElements, output);
      if (result == Core.RetCode.Success)
      {
        if (padPrePeriodResults)
        {
          return Enumerable.Repeat(defaultPadValue, outBeginIndex).Concat(output);
        }
        return output;
      }
      else
      {
        throw new Exception(Enum.GetName(result.GetType(), result));
      }
    }

    //var i = 0;
    //foreach (var x in set)
    //{
    //  ema = Statistic.CalculateEma(ema, alpha, x);
    //  if (i++ < period)
    //  {
    //    yield return null;
    //  }
    //  else
    //  {
    //    yield return ema;
    //  }
    //}
    // }
    public static IEnumerable<double> Tema2(this IEnumerable<double> source, int period = 13)
    {
      Contract.Requires<ArgumentNullException>(source != null);
      Contract.Requires<ArgumentOutOfRangeException>(period > 0, "TEMA period must be more than 0");
      Contract.Requires<ArgumentOutOfRangeException>(source.Count() >= period);
      //Contract.Requires<ArgumentOutOfRangeException>(autodetectSmoothingFactor && alphaDecimalPrecision > 0);
      //Contract.Requires<ArgumentOutOfRangeException>(autodetectSmoothingFactor && alphaPrecisionBase > 1);

      //if (!autodetectSmoothingFactor)
      //{
      var alpha = 2 / (double)(1 + period);
      throw new NotImplementedException("Dummy based on Statistic.CalculateEma(...) function");
      // return current EMA
      var emas = source
        .Scan((prevEma, x) => Statistic.CalculateEma(x, prevEma, alpha));

      return emas;
    }

    public static IEnumerable<TResult> Zip<TOuter, TInner, TResult>(this IEnumerable<TOuter> outerSeries, Func<TOuter, TInner[], TResult> selector, params IEnumerable<TInner>[] innerSeries)
    {
      var os = outerSeries.First();
      var iss = innerSeries.First().ToArray();
      var items = selector(os, iss);
      throw new NotImplementedException();
    }

    #region Alpha autodetection

    //      else
    //      {
    //        var precicions = Enumerable.Range(1, alphaDecimalPrecision)
    //          .Select(p => 1 / Math.Pow(alphaPrecisionBase, p))
    //          .ToArray();

    //        var testAlpha = double.Epsilon;
    //        var bestEma = new List<double>();
    //        var minMse = double.MaxValue;

    //#if DEBUG
    //        var bestAlpha = testAlpha; // redundant
    //        var sw = new System.Diagnostics.Stopwatch();
    //        sw.Start();
    //#endif
    //        for (int i = 0; i < precicions.Length; i++)
    //        {
    //          for (int j = 0; j < alphaPrecisionBase && testAlpha < 1.0; testAlpha += precicions[i], j++)
    //          {
    //            var emas = set
    //              .Scan(ema, (prevEma, x) => Statistic.CalculateEma(x, prevEma, testAlpha))
    //              .ToList(); // memory hog. Alternative: store only bestAlpha and then recalculate EMA(bestAlpha) -> more CPU, less RAM
    //            var mse = Statistic.MeanSquareError(set, emas);
    //            if (mse < minMse)
    //            {
    //              minMse = mse;
    //#if DEBUG
    //              bestAlpha = testAlpha; // redundant
    //#endif
    //              bestEma = emas;
    //            }
    //          }
    //        }
    //#if DEBUG
    //        sw.Stop();
    //        System.Diagnostics.Trace.WriteLine(string.Format("Best alpha: {0} calculated in {1}", bestAlpha, sw.Elapsed));
    //#endif

    //        return bestEma;
    //}

    #endregion Alpha autodetection

    private static int CalculateAllocationSize(int endIndex, int temp)
    {
      Contract.Requires<ArgumentOutOfRangeException>(endIndex >= 0);
      Contract.Requires<ArgumentOutOfRangeException>(temp >= 0);
      Contract.Requires<ArgumentOutOfRangeException>(temp <= endIndex ? endIndex - temp + 1 <= int.MaxValue : true);
      Contract.Ensures(Contract.Result<int>() >= 0);
      Contract.Ensures(Contract.Result<int>() <= int.MaxValue);

      var allocationSize = temp <= endIndex
        ? endIndex - temp + 1
        : 0;
      return allocationSize;
    }
  }
}