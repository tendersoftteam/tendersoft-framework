using System;
using Tendersoft.Distributions.MersenneTwister;

namespace Tendersoft.Distributions.NormDist
{
  public class NormalDistribution : System.Random
  {
    private double mean;
    public double Mean
    {
      get { return mean; }
    }

    private double deviation;
    public double Deviation
    {
      get { return deviation; }
    }

    private double x1, x2, w, y1;
    private static double y2;
    private static bool useLast = false;
    private static bool y1mean = false;

    private static Random rnd;
    private static MT19937 rndMT;

    private static bool useSystemRandom = true;
    public bool UseSystemRandom
    {
      get { return useSystemRandom; }
      set { useSystemRandom = value; }
    }

    public NormalDistribution(int seed, double mean, double deviation)
    {
      rnd = new Random(seed);
      rndMT = new MT19937((ulong)seed);
      this.mean = mean;
      this.deviation = deviation;
    }

    public NormalDistribution(double mean, double deviation)
      : this(0, mean, deviation)
    {
      rnd = new Random();
      rndMT = new MT19937((ulong)rnd.Next());
    }

    public NormalDistribution()
      : this(0.0, 1.0)
    {
    }

    public double GaussDouble()
    {
      if (useLast)		        /* use value from previous call */
      {
        y1 = y2;
        useLast = false;
      }
      else
      {
        do
        {
          if (useSystemRandom)
          {
            x1 = 2.0 * rnd.NextDouble() - 1.0;
            x2 = 2.0 * rnd.NextDouble() - 1.0;
          }
          else
          {
            x1 = 2.0 * rndMT.genrand_res53() - 1.0;
            x2 = 2.0 * rndMT.genrand_res53() - 1.0;
          }
          w = x1 * x1 + x2 * x2;
        } while (w >= 1.0 || w == 0.0);

        w = Math.Sqrt((-2.0 * Math.Log(w, Math.E)) / w);
        y1 = x1 * w;
        y2 = x2 * w;
        useLast = true;
      }

      //if (Math.Abs(mean + y2 * deviation) - 1 <= 0.0)
      //{
      //    useLast = false;
      //}
      //return Math.Abs(mean + y1 * deviation) - 1e-3 <= 0.0 ? GeneratorBase() : (mean + y1 * deviation);
      return (mean + y1 * deviation);
    }

    public override int Next()
    {
      return this.Next(int.MaxValue);
    }

    public override int Next(int maxValue) /* throws ArgumentOutOfRangeException */
    {
      if (maxValue <= 1)
      {
        if (maxValue < 0)
        {
          throw new ArgumentOutOfRangeException();
        }

        return 0;
      }

      return (int)(this.NextDouble() * maxValue);
    }

    public override int Next(int minValue, int maxValue)
    {
      if (maxValue < minValue)
      {
        throw new ArgumentOutOfRangeException();
      }
      else if (maxValue == minValue)
      {
        return minValue;
      }
      else
      {
        return this.Next(maxValue - minValue) + minValue;
      }
    }

    public override void NextBytes(byte[] buffer) /* throws ArgumentNullException*/
    {
      int bufLen = buffer.Length;

      if (buffer == null)
      {
        throw new ArgumentNullException();
      }

      for (int idx = 0; idx < bufLen; ++idx)
      {
        buffer[idx] = (byte)this.Next(256);
      }
    }

    public override double NextDouble()
    {
      return this.GaussDouble();
    }
  }
}