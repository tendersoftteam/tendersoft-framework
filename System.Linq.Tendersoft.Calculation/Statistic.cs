using System.Collections.Generic;
using System.Reactive.Linq;
using Tendersoft.Calculation;
using System.Diagnostics.Contracts;

namespace System.Linq.Tendersoft.Calculation
{
  public static class Statistic
  {
    private static Random r = new Random();

    public static Random Randomizer { get { return r; } }

    public static int AverageConstrained(int[] proba, float accuracy)
    {
      return AverageConstrained(proba, accuracy, true);
    }

    public static int AverageConstrained(int[] proba, float accuracy, bool doBackup)
    {
      int[] tempProba;
      if (doBackup)
      {
        // make copy
        tempProba = new int[proba.Length];
        for (int i = 0; i < tempProba.Length; i++)
        {
          tempProba[i] = proba[i];
        }
      }
      else
      {
        // don't make copy
        tempProba = proba;
      }

      int srednia = (int)tempProba.Average();
      double odchStandard = CalculateDeviantEst(tempProba, srednia);

      while (accuracy > 1.0F - (float)odchStandard / (float)srednia)
      {
        RemoveDeviantPoints(tempProba, srednia, (int)odchStandard);
        srednia = (int)tempProba.Average();
        odchStandard = CalculateDeviantEst(tempProba, srednia);
      }

      return srednia;
    }

    public static int AverageConstrained(double maxDeviance, int[] proba)
    {
      return AverageConstrained(maxDeviance, proba, true);
    }

    public static int AverageConstrained(double maxDeviance, int[] proba, bool doBackup)
    {
      int[] tempProba;
      if (doBackup)
      {
        // make copy
        tempProba = proba.ToArray();
      }
      else
      {
        // don't make copy
        tempProba = proba;
      }

      int srednia = (int)tempProba.Average();
      double odchStandard = CalculateDeviantEst(tempProba, srednia);

      while (odchStandard > maxDeviance)
      {
        RemoveDeviantPoints(tempProba, srednia, (int)odchStandard);
        srednia = (int)tempProba.Average();
        odchStandard = CalculateDeviantEst(tempProba, srednia);
      }

      return srednia;
    }

    public static double CalculateDeviantEst(List<int> coordStat, int average)
    {
      ulong sum = 0;
      foreach (int coord in coordStat)
      {
        int substract = coord - average;
        sum += (ulong)(substract * substract);
      }
      double division = (double)sum / (double)(coordStat.Count - 1);
      return Math.Sqrt(division);
    }

    public static double CalculateDeviantEst(int[] coordStat, int average)
    {
      ulong sum = 0;
      for (int i = 0; i < coordStat.Length; i++)
      {
        int substract = coordStat[i] - average;
        sum += (ulong)(substract * substract);
      }

      double division = (double)sum / (double)(coordStat.Length - 1);
      return Math.Sqrt(division);
    }

    //public static IEnumerable<decimal> Rsi(this IEnumerable<decimal> set, int period)
    //{
    //    TicTacTec.TA.Library.Core.Rsi(
    //}
    public static int CalculateMedianIndex(int resultsCount)
    {
      var medianIndex = resultsCount == 1
          ? 0
          : (resultsCount % 2 == 1
              ? resultsCount / 2 + 1
              : resultsCount / 2);

      return medianIndex;
    }

    public static double EvaluateFunctionBetweenPoints(Tuple<double, double> p1, Tuple<double, double> p2, double x)
    {
      var slopeIntercept = LinearReg.CalculateSlopeIntercept(p1, p2);
      return slopeIntercept.Item1 * x + slopeIntercept.Item2;
    }

    /// <summary>
    /// Generates array of size <paramref name="upperLimit"/> with values {0;<paramref name="upperLimit"/>-1} scattered randomly inside the array.
    /// Each value appears in the array only once.
    /// </summary>
    /// <param name="upperLimit">number of elements in the array and the higherst value in the array</param>
    /// <returns>array of random distinct numbers from set {0;<paramref name="upperLimit"/>-1}</returns>
    public static int[] GenerateUniqueRandomArray(int upperLimit)
    {
      Contract.Requires<ArgumentOutOfRangeException>(upperLimit > 0);
      Contract.Ensures(
        Contract.ForAll(
          Contract.Result<int[]>(),
          x => Contract.Result<int[]>().Count(y => x == y) == 1
        )
      );

      return Enumerable.Range(0, upperLimit)
          .Select((x, index) => new { Index = index, RandomValue = r.NextDouble() })
          .OrderBy(x => x.RandomValue)
          .Select(x => x.Index)
          .ToArray();
    }

    public static double LogisticBipolarFunction(double input, params double[] funcParams)
    {
      double exp = Math.Exp(-funcParams[0] * input).ProtectFromInfinity();
      double divident = (1.0 - exp).ProtectFromInfinity();
      double divisor = (1.0 + exp).ProtectFromInfinity();
      if (divisor == 0.0 && divident == 0.0)
      {
        throw new ArithmeticException("NaN in Bi-polar logistic function because 0/0");
      }

      return ((1 - exp) / (1 + exp)).ProtectFromInfinity();
    }

    public static double LogisticBipolarFunctionDerivative(Func<double, double, double> activeFunc, double input, params double[] deriveParams)
    {
      double y = activeFunc(input, deriveParams[0]);
      return (1.0 + y) * (1.0 - y);
    }

    /// <summary>
    /// Returns is the middle number of a set of numbers
    /// </summary>
    /// <param name="set"></param>
    /// <returns></returns>
    public static decimal Median(this IEnumerable<decimal> set)
    {
      int numberCount = set.Count();
      int halfIndex = numberCount / 2;
      var sortedNumbers = set
          .OrderBy(n => n)
          .ToList();
      decimal median;
      if ((numberCount % 2) == 0)
      {
        median = ((sortedNumbers.ElementAt(halfIndex) +
            sortedNumbers.ElementAt((halfIndex - 1))) / 2M);
      }
      else
      {
        median = sortedNumbers.ElementAt(halfIndex);
      }
      return median;
    }

    public static double Median(this IEnumerable<double> set)
    {
      return (double)set
          .Select(x => (decimal)x)
          .Median();
    }

    /// <summary>
    /// Returns is the middle number of a set of numbers
    /// </summary>
    /// <param name="set"></param>
    /// <returns></returns>
    public static decimal Median(this IObservable<decimal> set)
    {
      var set2 = set.ToEnumerable();
      return set2.Median();
    }

    /// <summary>
    /// Returns the number that occurs the largest number of times
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="set"></param>
    /// <returns></returns>
    public static T Mode<T>(this IEnumerable<T> set)
    {
      var mode = set.GroupBy(n => n)
          .OrderByDescending(g => g.Count())
          .Select(g => g.Key)
          .FirstOrDefault();

      return mode;
    }

    /// <summary>
    /// Changes double.PositiveInfinity into double.MaxValue and double.NegativeInfinity into double.MinValue
    /// </summary>
    /// <param name="num">value to be checked</param>
    /// <returns>value in range [double.MaxValue;double.MinValue]</returns>
    public static double ProtectFromInfinity(this double num)
    {
      if (double.IsInfinity(num))
      {
        if (double.IsPositiveInfinity(num))
        {
          num = double.MaxValue;
        }
        else
        {
          num = double.MinValue;
        }
      }
      return num;
    }

    public static void RemoveDeviantPoints(List<int> coordStat, int average, int epsilon)
    {
      int index = 0;
      while (index < coordStat.Count)
      {
        if (Math.Abs(average - coordStat[index]) > epsilon)
        {
          coordStat.RemoveAt(index);
        }
        else
        {
          index++;
        }
      }
    }

    public static void RemoveDeviantPoints(int[] coordStat, int average, int epsilon)
    {
      for (int i = 0; i < coordStat.Length; i++)
      {
        if (Math.Abs(average - coordStat[i]) > epsilon)
        {
          coordStat[i] = average;
        }
      }
    }
    public static double StdDev(this IEnumerable<double> list)
    { return list.StdDev(x => x); }

    public static double StdDev<T>(this IEnumerable<T> list, Func<T, double> values)
    {
      // ref: http://stackoverflow.com/questions/2253874/linq-equivalent-for-standard-deviation
      // ref: http://warrenseen.com/blog/2006/03/13/how-to-calculate-standard-deviation/
      // ref: http://stackoverflow.com/a/12429178
      var mean = 0.0;
      var sum = 0.0;
      var stdDev = 0.0;
      var n = 0;
      foreach (var value in list.Select(values))
      {
        n++;
        var delta = value - mean;
        mean += delta / n;
        sum += delta * (value - mean);
      }
      if (1 < n)
        stdDev = Math.Sqrt(sum / (n - 1));

      return stdDev;
    }

    /// <summary>
    /// Calculates current Exponential Moving Average using previous EMA, apha and previous X
    /// </summary>
    /// <param name="currentX"></param>
    /// <param name="previousEma"></param>
    /// <param name="alpha">The coefficient alptha represents the degree of weighting decrease, a constant smoothing factor between 0 and 1. A higher alpha discounts older observations faster.</param>
    /// <returns></returns>
    internal static double CalculateEma(double currentX, double previousEma, double alpha)
    {
      Contract.Requires<ArgumentOutOfRangeException>(alpha > 0);
      Contract.Requires<ArgumentOutOfRangeException>(alpha < 1);

      var currentEma = currentX * alpha + previousEma * (1 - alpha);
      return currentEma;
    }

    internal static double GenerateWeigth()
    {
      double result;
      do
      {
        result = Randomizer.NextDouble() * 2.0 - 1.0;
      } while (result <= 0.0 + double.Epsilon && result >= 0.0 - double.Epsilon);
      return result;
    }

    internal static double GenerateWeigth(int dendriteCount)
    {
      double w0 = 1.0 / (double)dendriteCount;
      if (double.IsNaN(w0))
      {
        throw new ArithmeticException("weight cannot be NaN.");
      }
      return w0;
    }

    public static double MeanSquareError(IEnumerable<double> series, IEnumerable<double> predictions)
    {
      Contract.Requires<ArgumentNullException>(series != null);
      Contract.Requires<ArgumentNullException>(predictions != null);
      Contract.Requires<ArgumentException>(series.Any());
      Contract.Requires<ArgumentException>(predictions.Any());
      Contract.Ensures(Contract.Result<double>() >= 0);

      var err = series
        .Zip(predictions, (s, p) => Math.Pow(s - p, 2))
        .ToList();
      var result = err.Sum() / err.Count;

      return result;
    }
  }
}