using System;
using System.Collections.Generic;

//using System.Threading;

namespace Tendersoft.Calculation
{
  public static class Correlation
  {
    [Obsolete]
    public static double PearsonCorrelation(List<double> x, List<double> y)
    {
      if (x.Count != y.Count)
      {
        throw new ArrayTypeMismatchException();
      }

      if (x.Count < 1)
      {
        throw new ArgumentException();
      }

      //
      // Mean
      //
      double xMean = 0.0;
      double yMean = 0.0;

      foreach (double xi in x)
      {
        xMean += xi;
      }
      xMean /= x.Count;

      foreach (double yi in y)
      {
        yMean += yi;
      }
      yMean /= y.Count;

      //
      // numerator and denominator
      //

      double sumXDevYDev = 0.0;
      double sumXDev2 = 0.0;
      double sumYDev2 = 0.0;
      double xDev = 0.0;
      double yDev = 0.0;

      for (int i = 0; i < x.Count; i++)
      {
        xDev = x[i] - xMean;
        yDev = y[i] - yMean;

        sumXDevYDev += xDev * yDev;

        sumXDev2 += xDev * xDev;
        sumYDev2 += yDev * yDev;
      }

      // to avoid the unlikely event of double's precision mismatch instead of:
      // sumXDev2 == 0.0 || sumYDev2 == 0.0 we use the following
      if (Math.Abs(sumXDev2) < double.Epsilon || Math.Abs(sumYDev2) < double.Epsilon)
      {
        return 0;
      }
      else
      {
        return sumXDevYDev / (Math.Sqrt(sumXDev2) * Math.Sqrt(sumYDev2));
      }
    }

    [Obsolete]
    public static decimal PearsonCorrelation(List<decimal> x, List<decimal> y)
    {
      if (x.Count != y.Count)
      {
        throw new ArrayTypeMismatchException();
      }

      if (x.Count < 1)
      {
        throw new ArgumentException();
      }

      //
      // Mean
      //
      decimal xMean = 0;
      decimal yMean = 0;

      foreach (decimal xi in x)
      {
        xMean += xi;
      }
      xMean /= x.Count;

      foreach (decimal yi in y)
      {
        yMean += yi;
      }
      yMean /= y.Count;

      //
      // numerator and denominator
      //

      decimal sumXDevYDev = 0;
      decimal sumXDev2 = 0;
      decimal sumYDev2 = 0;
      decimal xDev = 0;
      decimal yDev = 0;

      for (int i = 0; i < x.Count; i++)
      {
        xDev = x[i] - xMean;
        yDev = y[i] - yMean;

        sumXDevYDev += xDev * yDev;

        sumXDev2 += xDev * xDev;
        sumYDev2 += yDev * yDev;
      }

      if (sumXDev2 == 0 || sumYDev2 == 0)
      {
        return 0;
      }
      else
      {
        return sumXDevYDev / (decimal)(Math.Sqrt((double)sumXDev2) * Math.Sqrt((double)sumYDev2));
      }
    }

    internal static double Mean(IList<double> x)
    {
      double mean = 0.0;
      foreach (double xi in x)
      {
        mean += xi;
      }
      return mean / x.Count;
    }

    internal static double SumXX2(IList<double> x, out double sumX2)
    {
      double sumX = 0.0;
      sumX2 = 0.0;
      foreach (double xi in x)
      {
        sumX += xi;
        sumX2 += xi * xi;
      }
      return sumX;
    }

    internal static double SumXY(IList<double> x, IList<double> y)
    {
      double sumXY = 0.0;
      for (int i = 0; i < x.Count; i++)
      {
        sumXY += x[i] * y[i];
      }
      return sumXY;
    }

    [Obsolete]
    public static int CpuCount()
    {
      return Environment.ProcessorCount;
    }

    private static CorrelResult CorrelationSubStream(List<double> x, List<double> y, int i0, int j0, int period)
    {
      #region Argument Checking

      if (period > x.Count || period > y.Count)
      {
        throw new ArgumentOutOfRangeException("period", period, "");
      }
      if (i0 >= x.Count)
      {
        throw new ArgumentOutOfRangeException("i0", i0, "");
      }
      if (i0 + period > x.Count)
      {
        throw new ArgumentOutOfRangeException("i0 + period", i0, "period=" + period.ToString());
      }
      if (j0 >= y.Count)
      {
        throw new ArgumentOutOfRangeException("j0", j0, "");
      }
      if (j0 + period > y.Count)
      {
        throw new ArgumentOutOfRangeException("j0 + period", j0, "period=" + period.ToString());
      }

      #endregion Argument Checking

      double sumX2 = 0.0, sumX = 0.0, sumX_2_n = 0.0, sumY2 = 0.0, sumY = 0.0, sumY_2_n = 0.0, sumXY = 0.0;

      for (int i = i0, j = j0; i < i0 + period && j < j0 + period; i++, j++)
      {
        sumX += x[i];
        sumX2 += x[i] * x[i];

        sumY += y[j];
        sumY2 += y[j] * y[j];

        sumXY += x[i] * y[j];
      }
      sumX_2_n = (sumX * sumX) / period;
      sumY_2_n = (sumY * sumY) / period;
      //return (sumXY - ((sumX * sumY) / period)) / Math.Sqrt((sumX2 - sumX_2_n) * (sumY2 - sumY_2_n));
      return new CorrelResult(i0, j0, period, (sumXY - ((sumX * sumY) / period)) / Math.Sqrt((sumX2 - sumX_2_n) * (sumY2 - sumY_2_n)));
    }

    public static IList<CorrelResult> CorrelationStream(List<double> x, List<double> y, int period)
    {
      List<CorrelResult> correlResultsCalculated = new List<CorrelResult>((x.Count - period) * (y.Count - period));
      for (int i = 0; i < x.Count - period; i++)
      {
        for (int j = 0; j < y.Count - period; j++)
        {
          //correlResultsCalculated.Add(new CorrelResultCalc(i, j, period, CorrelationSumStream(x, y, i, j, period)));
          correlResultsCalculated.Add(CorrelationSubStream(x, y, i, j, period));
        }
      }
      return correlResultsCalculated;
    }
  }

  public class CorrelResult
  {
    private CorrelResult()
    {
    }

    public CorrelResult(int start1, int start2, int period, double correlation)
    {
      this.sample1StartPoint = start1;
      this.sample2StartPoint = start2;
      this.period = period;
      this.correlation = correlation;
    }

    private int sample1StartPoint;
    public int Sample1StartPoint
    {
      get { return sample1StartPoint; }
    }

    private int sample2StartPoint;
    public int Sample2StartPoint
    {
      get { return sample2StartPoint; }
    }

    private int period;
    public int Period
    {
      get { return period; }
    }

    private double correlation;
    public double Correlation
    {
      get { return correlation; }
    }
  }
}