﻿// -----------------------------------------------------------------------
// <copyright file="NaiveBayesProgram.cs" company="">
// TODO: Update copyright text.
// </copyright>
// -----------------------------------------------------------------------

namespace TestConsole
{
  using System;
  using System.Linq;
  using System.Reflection.Tendersoft;
  using System.Runtime.Serialization;
  using System.Text;
  using Tendersoft.Analysis.Classifiers;

  /// <summary>
  /// TODO: Update summary.
  /// </summary>
  public class NaiveBayesProgram
  {
    protected internal enum TestClassGender
    {
      Male,
      Female
    }

    protected internal class TestClass
    {
      public string Name { get; set; }

      public int Age { get; set; }

      public decimal Height { get; set; }

      public bool IsRightHanded { get; set; }

      public TestClassGender Gender { get; set; }

      [IgnoreDataMember]
      public byte SomeProperty { get; set; }

      private int SomeOtherProperty { get; set; }

      public override string ToString()
      {
        var str0 = this.GetMemberPropertyInfos()
          .Select(pi => new { Name = pi.Name, Value = pi.GetValue(this, null) })
          .Aggregate
          (
            new StringBuilder(),
            (sb, p) => sb.Append(string.Format("{0}:{1},", p.Name, p.Value)),
            sb => sb.ToString().Substring(0, sb.Length - 1)
          );

        //var str = string.Format("{0}, {1}, {2}, {3}, {4}, {5}, {6}", Name, Age, Height, IsRightHanded, Gender, SomeProperty, SomeOtherProperty);
        return str0;
      }
    }

    static internal void Test()
    {
      var trainingData = Enumerable.Empty<TestClass>();
      var classifier = new NaiveBayesClassifier<TestClass>();
      trainingData = MakeData(40000);

      Console.WriteLine("First 5 lines of training data are:\n");

      foreach (var item in trainingData.Take(5))
      {
        Console.WriteLine(item);
      }
      Console.WriteLine("\n");

      classifier = new NaiveBayesClassifier<TestClass>(trainingData);
      var dataDefinition = new DataDefinition<TestClass>();

      dataDefinition.ClassesDefinitions
        .Add(
          x => x.Age,
          new ClassDefinition<TestClass>(x => x.Age, new Accord.Statistics.Distributions.Univariate.GeneralDiscreteDistribution())
        );
      dataDefinition.ClassesDefinitions
        .Add(
          x => x.Gender,
          new ClassDefinition<TestClass>(x => x.Gender, new Accord.Statistics.Distributions.Univariate.GeneralDiscreteDistribution(2))
        );
      dataDefinition.ClassesDefinitions
        .Add(
          x => x.Height,
          new ClassDefinition<TestClass>(x => x.Height, new Accord.Statistics.Distributions.Univariate.NormalDistribution())
        );
      dataDefinition.ClassesDefinitions
        .Add(
          x => x.IsRightHanded,
          new ClassDefinition<TestClass>(x => x.IsRightHanded, new Accord.Statistics.Distributions.Univariate.GeneralDiscreteDistribution(2))
        );
      dataDefinition.ClassesDefinitions
        .Add(
          x => x.Name,
          new ClassDefinition<TestClass>(x => x.Name, new Accord.Statistics.Distributions.Univariate.GeneralDiscreteDistribution())
        );
      classifier = new NaiveBayesClassifier<TestClass>(dataDefinition, trainingData);

      var testEntity = new TestClass() { Gender = TestClassGender.Female, Name = "education", IsRightHanded = true, Height = 1.9M, Age = 16, SomeProperty = 1 };
      //string occupation = "education";
      //string dominance = "right";
      //string height = "tall"; };
      var result = classifier.Classify(testEntity, x => x.Age, x => x.Age >= 16);
      foreach (var item in result)
      {
        Console.WriteLine(item);
      }
    }

    private static TestClass[] MakeData(int numRows) // make dummy data
    {
      var result = new TestClass[numRows];
      var r = new Random();
      for (int i = 0; i < numRows; ++i)
      {
        var s = new TestClass() { Gender = MakeSex() };
        s.Height = (decimal)MakeHeight(s.Gender);
        s.IsRightHanded = MakeDominance(s.Gender);
        s.Name = MakeOccupation(s.Gender);
        s.Age = r.Next(60);
        result[i] = s;
      }
      return result;
    }

    private static TestClassGender MakeSex()
    {
      var ran = new Random();
      int r = ran.Next(0, 19);
      if (r <= 11) return TestClassGender.Male; // 60%
      else return TestClassGender.Female; // 40%
    }

    private static bool MakeDominance(TestClassGender sex)
    {
      var ran = new Random();
      double p = ran.NextDouble();
      switch (sex)
      {
        case TestClassGender.Male:
          if (p < 0.33) return false; else return true;
          break;

        case TestClassGender.Female:
          if (p < 0.20) return false; else return true;
          break;

        default:
          break;
      }

      throw new NotImplementedException();
    }

    private static string MakeOccupation(TestClassGender sex)
    {
      var ran = new Random();
      int r = ran.Next(0, 20);
      switch (sex)
      {
        case TestClassGender.Male:
          if (r == 0) return "administrative"; // 5%
          else if (r >= 1 && r <= 6) return "construction"; // 30%
          else if (r >= 7 && r <= 9) return "education"; // 15%
          else if (r >= 10 && r <= 19) return "technology"; // 50%
          break;

        case TestClassGender.Female:
          if (r >= 0 & r <= 9) return "administrative"; // 50%
          else if (r == 10) return "construction"; // 5%
          else if (r >= 11 & r <= 15) return "education";  // 25%
          else if (r >= 16 && r <= 19) return "technology"; // 20%
          break;

        default:
          break;
      }

      throw new NotImplementedException();
    }

    private static double MakeHeight(TestClassGender sex)
    {
      var ran = new Random();
      int bucket = 0;  // height bucket: 0 = short, 1 = medium, 2 = tall
      double p = ran.NextDouble();
      if (p < 0.1587) bucket = 0;
      else if (p > 0.8413) bucket = 2;
      else bucket = 1; // p = (2 * 0.3413) = 0.6826

      double hi = 0.0;
      double lo = 0.0;

      switch (sex)
      {
        case TestClassGender.Male:
          if (bucket == 0) { lo = 1.4; hi = 1.6; }
          else if (bucket == 1) { lo = 1.6; hi = 1.8; }
          else if (bucket == 2) { lo = 1.8; hi = 2.2; }
          break;

        case TestClassGender.Female:
          if (bucket == 0) { lo = 1.3; hi = 1.4; }
          else if (bucket == 1) { lo = 1.4; hi = 1.6; }
          else if (bucket == 2) { lo = 1.6; hi = 2.0; }
          break;

        default:
          break;
      }

      double resultAsDouble = (hi - lo) * ran.NextDouble() + lo;
      return resultAsDouble;
      //return resultAsDouble.ToString("F1");
    }
  }
}