﻿using System.Collections.Generic;
using System.Diagnostics.Contracts;
using System.Linq;
using System.Linq.Expressions;

namespace System.Reflection.Tendersoft
{
  public static class ReflectionExtensions
  {
    private static Dictionary<Tuple<Type, Type[]>, MethodInfo> op_ExplicitCache = new Dictionary<Tuple<Type, Type[]>, MethodInfo>();

    /// <summary>
    ///
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <typeparam name="TResult"></typeparam>
    /// <param name="obj"></param>
    /// <returns></returns>
    /// <remarks>thread safe</remarks>
    public static TResult CastTo<T, TResult>(this T obj)
    {
      lock (op_ExplicitCache)
      {
        var opHashObj = Tuple.Create(typeof(TResult), new[] { typeof(T) });

        MethodInfo castOperator;
        op_ExplicitCache.TryGetValue(opHashObj, out castOperator);
        if (castOperator == null)
        {
          castOperator = opHashObj.Item1.GetMethod("op_Explicit", opHashObj.Item2);
        }
        if (castOperator != null)
        {
          op_ExplicitCache.Add(opHashObj, castOperator);
          return (TResult)castOperator.Invoke(null, new object[] { obj });
        }

        opHashObj = Tuple.Create(typeof(T), new[] { typeof(T) });
        op_ExplicitCache.TryGetValue(opHashObj, out castOperator);
        if (castOperator == null)
        {
          castOperator = opHashObj.Item1.GetMethod("op_Explicit", opHashObj.Item2);
        }
        if (castOperator != null)
        {
          op_ExplicitCache.Add(opHashObj, castOperator);
          return (TResult)castOperator.Invoke(null, new object[] { obj });
        }
      }

      throw new InvalidCastException("Can't cast " + typeof(T).Name + " to " + typeof(TResult).Name);
    }

    public static string GetMemberName<T>(this T entity, Expression<Func<T, object>> expression)
    {
      Contract.Requires(expression != null);

      MemberExpression memberExpression = GetMemberExpression(expression);
      var propertyInfo = memberExpression.Member as PropertyInfo;
      if (propertyInfo == null) return string.Empty;
      return propertyInfo.Name;
    }

    public static PropertyInfo GetMemberPropertyInfo<T, TValue>(this T entity, Expression<Func<T, TValue>> expression)
    {
      Contract.Requires(expression != null);

      MemberExpression memberExpression = GetMemberExpression(expression);
      var propertyInfo = memberExpression.Member as PropertyInfo;

      return propertyInfo;
    }

    public static TValue GetMemberValue<T, TValue>(this T entity, Expression<Func<T, TValue>> expression)
    {
      Contract.Requires(expression != null);

      MemberExpression memberExpression = GetMemberExpression(expression);
      var propertyInfo = memberExpression.Member as PropertyInfo;
      if (propertyInfo != null)
      {
        return (TValue)propertyInfo.GetValue(entity, null);
      }

      throw new ArgumentException("Provided expression tree does not evaluate to a valid PropertyInfo.");
    }

    public static MemberExpression GetMemberExpression<T, TValue>(Expression<Func<T, TValue>> expression)
    {
      Contract.Requires<NullReferenceException>(expression != null);

      if (expression.Body is MemberExpression)
      {
        return (MemberExpression)expression.Body;
      }
      if (expression.Body is UnaryExpression)
      {
        var operand = ((UnaryExpression)expression.Body).Operand;
        if (operand is MemberExpression)
        {
          return (MemberExpression)operand;
        }
        if (operand is MethodCallExpression)
        {
          return ((MethodCallExpression)operand).Object as MemberExpression;
        }
      }

      throw new ArgumentException("Provided expression does not contain a suitable element of type MemberExpression.");
    }

    public static PropertyInfo[] GetMemberPropertyInfos<T>(this T entity)
    {
      Contract.Requires<NullReferenceException>(entity != null);

      return entity.GetType().GetProperties();
    }

    public static IEnumerable<string> GetMemberPropertyNames<T>(this T entity)
    {
      Contract.Requires<NullReferenceException>(entity != null);

      return entity.GetMemberPropertyInfos().Select(p => p.Name);
    }
  }
}