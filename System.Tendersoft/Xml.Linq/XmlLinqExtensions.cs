﻿using System.Collections.Generic;
using System.Linq;
using System.Diagnostics.Contracts;

namespace System.Xml.Linq.Tendersoft
{
  public static class XmlLinqExtensions
  {
    //public void SetAttributeValue<T>(this XElement baseElement, Action<T> predicate)
    //{
    //    baseElement.SetAttributeValue()
    //}

    //public static void SetValue<T>(this T entity, Expression<Func<T, object>> expression, object value)
    //  {
    //      MemberExpression memberExpression = GetMemberExpression(expression);
    //      var propertyInfo = memberExpression.Member as PropertyInfo;
    //      propertyInfo.SetValue(entity, value, null);
    //  }
    public static XElement GetElementByAttribute(this XDocument document, XAttribute attribute)
    {
      Contract.Requires<ArgumentNullException>(document != null);
      Contract.Requires<ArgumentNullException>(attribute != null);

      return document.GetElementByAttributes(attribute)
          .FirstOrDefault();
    }

    public static XElement GetElementByAttribute(this XDocument document, XName name, string value)
    {
      Contract.Requires<ArgumentNullException>(document != null);

      return document.GetElementByAttributes(name, value)
          .FirstOrDefault();
    }

    public static IEnumerable<XElement> GetElementByAttributes(this XDocument document, XAttribute attribute)
    {
      Contract.Requires<ArgumentNullException>(document != null);
      Contract.Requires<ArgumentNullException>(attribute != null);

      return document.GetElementByAttributes(attribute.Name, attribute.Value);
    }

    public static IEnumerable<XElement> GetElementByAttributes(this XDocument document, XName name, string value)
    {
      Contract.Requires<ArgumentNullException>(document != null);

      return document.Descendants()
         .Where(x => x.HasAttributes)
         .Where(x => x.Attributes().Any(a => a.Name == name && a.Value == value));
    }
  }
}