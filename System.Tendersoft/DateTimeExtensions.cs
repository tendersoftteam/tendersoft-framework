﻿using System.Diagnostics.Contracts;
namespace System.Tendersoft
{
  public static class DateTimeExtensions
  {
    private static DateTime unixEpochStart = new DateTime(1970, 1, 1, 0, 0, 0, 0);
    public static DateTime UnixEpochStart { get { return unixEpochStart; } }

    public static long UnixTimestamp(this DateTime t)
    {
      return (long)t.UnixEpochTime().TotalSeconds;
    }

    public static TimeSpan EpochTimeUtcNow()
    {
      return UnixEpochTime(DateTime.UtcNow);
    }

    public static TimeSpan UnixEpochTime(this DateTime t)
    {
      var unixEpochTs = t - unixEpochStart;
      return unixEpochTs;
    }

    //public static DateTime FromUnixTime(long unixTime)
    //{
    //    var unixEpoch = TimeSpanExtensions.FromUnixTime(unixTime);
    //    var fromUnixTime = unixEpochStart + unixEpoch;
    //    return fromUnixTime;
    //}

    public static DateTime FromUnixTimestamp(long timestamp)
    {
      return unixEpochStart.AddSeconds(timestamp);
    }

    [Obsolete]
    private static double ConvertToUnixTimestamp(DateTime date)
    {
      TimeSpan diff = date - unixEpochStart;
      return Math.Floor(diff.TotalSeconds);
    }

    /// <summary>
    /// The mean solar day is divided up into 1000 parts called ".beats". Each .beat lasts 1 minute and 26.4 seconds.
    /// Times are notated as a 3-digit number out of 1000 after midnight.
    /// https://en.wikipedia.org/wiki/Swatch_Internet_Time
    /// </summary>
    /// <param name="currentTime"></param>
    /// <returns>value in range [0;999]</returns>
    public static short ToSwatchTime(this DateTime currentTime)
    {
      Contract.Requires<ArgumentNullException>(currentTime != null);
      Contract.Ensures(Contract.Result<short>() >= 0);
      Contract.Ensures(Contract.Result<short>() < 1000);

      var utcPlus1 = currentTime.AddHours(1);
      // (UTC+1seconds + (UTC+1minutes * 60) + (UTC+1hours * 3600)) / 86,4
      var swatchTime = (byte)Math.Floor((utcPlus1.Second + utcPlus1.Minute * 60 + utcPlus1.Hour * 3600) / 86.4);

      return swatchTime;
    }
  }
}