﻿namespace System.Tendersoft
{
  public static class TimeSpanExtensions
  {
    internal const long ticksPerMicrosecond = TimeSpan.TicksPerMillisecond / 1000;

    /// <summary>
    /// Extension method provides microseconds resolution based on ticks
    /// </summary>
    /// <param name="ts">TimeSpan value</param>
    /// <returns>Microseconds (10^-6 sek.) contained in provided TimeSpan value</returns>
    public static long TotalMicroseconds(this TimeSpan ts)
    {
      var tm = ts.Ticks / ticksPerMicrosecond;
      return tm;
    }

    public static TimeSpan FromUnixTime(long unixTime)
    {
      var ticks = unixTime * TimeSpanExtensions.ticksPerMicrosecond;
      var unixEpoch = TimeSpan.FromTicks(ticks);
      var t0 = TimeSpan.FromMilliseconds(unixTime);
      var t1 = TimeSpan.FromSeconds(unixTime);
      return t1;// unixEpoch;
    }
  }
}