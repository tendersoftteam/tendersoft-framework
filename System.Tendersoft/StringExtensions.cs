﻿namespace System.Tendersoft
{
  using System.Diagnostics.Contracts;

  public static class StringExtensions
  {
    public static string FormatLine(this string str, string format, params object[] param)
    {
      Contract.Requires<ArgumentNullException>(param != null);

      return string.Format(format + "\n", param);
    }
  }
}