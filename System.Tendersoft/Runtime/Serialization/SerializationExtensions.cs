﻿using System.Diagnostics.Contracts;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

//using System.Runtime.Serialization.Formatters.Soap;

namespace System.Runtime.Serialization.Tendersoft
{
  public static class SerializationExtensions
  {
    public static void BinarySerialize<T>(this T obj, string filename)
    {
      Contract.Requires<ArgumentNullException>(obj != null);
      Contract.Requires<ArgumentException>(!string.IsNullOrWhiteSpace(filename));

      using (FileStream str = File.Create(filename))
      {
        BinaryFormatter bf = new BinaryFormatter();
        bf.Serialize(str, obj);
      }
    }

    public static T BinaryDeserialize<T>(string filename)
    {
      Contract.Requires<ArgumentException>(!string.IsNullOrWhiteSpace(filename));

      T result = default(T);
      using (FileStream str = File.OpenRead(filename))
      {
        BinaryFormatter bf = new BinaryFormatter();
        result = (T)bf.Deserialize(str);
      }
      return result;
    }

    /*
     * SoapFormatter is Obsolete since .NET 2.0
     */
    //private static void SoapSerialize<T>(T list, string filename)
    //{
    //    using (FileStream str = File.Create(filename))
    //    {
    //        SoapFormatter sf = new SoapFormatter();
    //        sf.Serialize(str, list);
    //    }
    //}

    //private static T SoapDeserialize<T>(string filename)
    //{
    //    T people = default(T);
    //    using (FileStream str = File.OpenRead(filename))
    //    {
    //        SoapFormatter sf = new SoapFormatter();
    //        people = (T)sf.Deserialize(str);
    //    }
    //    return people;
    //}
  }
}