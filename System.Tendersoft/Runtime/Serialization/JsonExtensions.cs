﻿// -----------------------------------------------------------------------
// <copyright file="JsonExtensions.cs" company="">
// TODO: Update copyright text.
// </copyright>
// -----------------------------------------------------------------------

namespace System.Runtime.Serialization.Tendersoft
{
  using System;
  using System.Diagnostics.Contracts;
  using System.IO;
  using Newtonsoft.Json;

  /// <summary>
  /// TODO: Update summary.
  /// </summary>
  public static class JsonExtensions
  {
    public static T CreateJsonFromStream<T>(this Stream stream)
    {
      Contract.Requires<ArgumentNullException>(stream != null);

      var serializer = new JsonSerializer();
      T data;
      using (var streamReader = new StreamReader(stream))
      {
        using (var reader = new JsonTextReader(streamReader))
        {
          data = serializer.Deserialize<T>(reader);
        }
      }
      return data;
    }

    public static T CreateJsonFromString<T>(this string json)
    {
      Contract.Requires<ArgumentNullException>(json != null);

      T data;
      using (var stream = new MemoryStream(System.Text.Encoding.Default.GetBytes(json)))
      {
        data = CreateJsonFromStream<T>(stream);
      }
      return data;
    }

    public static T CreateJsonFromFile<T>(this String fileName)
    {
      Contract.Requires<ArgumentNullException>(!string.IsNullOrEmpty(fileName));

      T data;
      using (var fileStream = new FileStream(fileName, FileMode.Open))
      {
        data = CreateJsonFromStream<T>(fileStream);
      }
      return data;
    }

    public static void WriteJsonToStream(this Stream stream, object graph)
    {
      Contract.Requires<ArgumentNullException>(stream != null);

      using (var sw = new StreamWriter(stream))
      {
        using (var writer = new JsonTextWriter(sw))
        {
          var serializer = new JsonSerializer();
          serializer.Serialize(writer, graph);
        }
      }
    }
  }
}