namespace System.Tendersoft
{
  public static class ObjectExtensions
  {
    /// <summary>
    /// Makes a copy from the object.
    /// Doesn't copy the reference memory, only data.
    /// </summary>
    /// <typeparam name="T">Type of the return object.</typeparam>
    /// <param name="item">Object to be copied.</param>
    /// <returns>Returns the copied object.</returns>
    public static T Clone<T>(this object item)
    {
      if (item != null)
      {
        System.Runtime.Serialization.Formatters.Binary.BinaryFormatter formatter = new System.Runtime.Serialization.Formatters.Binary.BinaryFormatter();
        using (System.IO.MemoryStream stream = new System.IO.MemoryStream())
        {
          formatter.Serialize(stream, item);
          stream.Seek(0, System.IO.SeekOrigin.Begin);

          T result = (T)formatter.Deserialize(stream);

          stream.Close();

          return result;
        }
      }
      else
        return default(T);
    }
  }
}