﻿namespace System.Timers.Tendersoft
{
  using System.Diagnostics.Contracts;

  public static class TimerExtensions
  {
    public static void Reset(this System.Timers.Timer t)
    {
      Contract.Requires<ArgumentNullException>(t != null);

      t.Stop();
      t.Start();
    }

    public static void Reset(this System.Timers.Timer t, double miliseconds)
    {
      Contract.Requires<ArgumentNullException>(t != null);

      t.Stop();
      t.Interval = miliseconds;
      t.Start();
    }

    public static void Reset(this System.Timers.Timer t, TimeSpan interval)
    {
      Contract.Requires<ArgumentNullException>(t != null);

      t.Stop();
      t.Interval = interval.TotalMilliseconds;
      t.Start();
    }
  }
}