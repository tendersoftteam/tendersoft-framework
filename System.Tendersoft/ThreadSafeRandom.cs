﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace System.Tendersoft
{
  class ThreadSafeRandom : Random
  {
    private static Random _global = new Random();
    [ThreadStatic]
    private static Random _local;

    public override int Next()
    {
      throw new NotImplementedException();
      return base.Next();
    }

    protected override double Sample()
    {
      throw new NotImplementedException();
      return base.Sample();
    }


    //public static int Next()
    //{
    //  throw new NotImplementedException();
    //  Random inst = _local;
    //  if (inst == null)
    //  {
    //    int seed;
    //    lock (_global) seed = _global.Next();
    //    _local = inst = new Random(seed);
    //  }
    //  return inst.Next();
    //}

  }

  /// <summary>
  /// a shitty version of somewhat theread-safe random based on: http://blogs.msdn.com/b/pfxteam/archive/2009/02/19/9434171.aspx
  /// </summary>
  public static class RandomGen2
  {
    private static Random _global = new Random();
    [ThreadStatic]
    private static Random _local;

    public static int Next()
    {
      Random inst = _local;
      if (inst == null)
      {
        inst = Initialize(inst);
      }
      return inst.Next();
    }

    public static double NextDouble()
    {
      Random inst = _local;
      if (inst == null)
      {
        inst = Initialize(inst);
      }
      return inst.NextDouble();
    }

    private static Random Initialize(Random inst)
    {
      int seed;
      lock (_global)
      {
        if (inst==null)
        {
          seed = _global.Next();
          _local = inst = new Random(seed); 
        }
      }
      return inst;
    }
  }
}
