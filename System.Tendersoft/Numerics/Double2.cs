﻿namespace System.Tendersoft.Numerics
{
  public static class Double2
  {
    public static double? TryParse(string s)
    {
      return Double2.TryParse(s, System.Globalization.CultureInfo.CurrentUICulture);
    }

    public static double? TryParse(string s, System.Globalization.CultureInfo ci)
    {
      double result;
      if (double.TryParse(s, System.Globalization.NumberStyles.Float, ci, out result))
      {
        return result;
      }
      else
      {
        return (double?)null;
      }
    }
  }
}