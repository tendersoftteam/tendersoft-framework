﻿namespace System.Tendersoft.Numerics
{
  public static class Int2
  {
    public static int? TryParse(string s)
    {
      return Int2.TryParse(s, System.Globalization.CultureInfo.CurrentUICulture);
    }

    public static int? TryParse(string s, System.Globalization.CultureInfo ci)
    {
      int result;
      if (int.TryParse(s, System.Globalization.NumberStyles.Integer, ci, out result))
      {
        return result;
      }
      else
      {
        return (int?)null;
      }
    }
  }
}