﻿namespace System.Tendersoft.Numerics
{
  public static class Long2
  {
    public static long? TryParse(string s)
    {
      return Long2.TryParse(s, System.Globalization.CultureInfo.CurrentUICulture);
    }

    public static long? TryParse(string s, System.Globalization.CultureInfo ci)
    {
      long result;
      if (long.TryParse(s, System.Globalization.NumberStyles.Integer, ci, out result))
      {
        return result;
      }
      else
      {
        return (long?)null;
      }
    }
  }
}