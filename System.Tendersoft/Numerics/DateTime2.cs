﻿namespace System.Tendersoft.Numerics
{
  public static class DateTime2
  {
    public static DateTime? TryParse(string s)
    {
      return DateTime2.TryParse(s, System.Globalization.CultureInfo.CurrentUICulture);
    }

    public static DateTime? TryParse(string s, System.Globalization.CultureInfo ci)
    {
      DateTime result;
      if (DateTime.TryParse(s, ci, System.Globalization.DateTimeStyles.None, out result))
      {
        return result;
      }
      else
      {
        return (DateTime?)null;
      }
    }

    public static DateTime? TryParseExact(string s, string format)
    {
      return DateTime2.TryParseExact(s, format, System.Globalization.CultureInfo.CurrentUICulture);
    }

    public static DateTime? TryParseExact(string s, string format, IFormatProvider provider)
    {
      DateTime result;
      if (DateTime.TryParseExact(s, format, provider, System.Globalization.DateTimeStyles.None, out result))
      {
        return result;
      }
      else
      {
        return (DateTime?)null;
      }
    }
  }
}