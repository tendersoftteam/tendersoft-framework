﻿// -----------------------------------------------------------------------
// <copyright file="Quaternion.cs" company="">
// TODO: Update copyright text.
// </copyright>
// -----------------------------------------------------------------------
using System.Reflection;
using System.Runtime.InteropServices;
using System.Runtime.Serialization;
using System.Diagnostics.Contracts;

namespace System.Tendersoft.Numerics
{
  [StructLayoutAttribute(LayoutKind.Sequential, CharSet = CharSet.Ansi)]
  [SerializableAttribute]
  public struct Quaternion : IEquatable<Quaternion>//, IFormattable, ISerializable
  {
    private readonly double r, i, j, k;
    public double R { get { return r; } }
    public double I { get { return i; } }
    public double J { get { return j; } }
    public double K { get { return k; } }

    public Quaternion(double r, double i, double j, double k)
    {
      this.r = r;
      this.i = i;
      this.j = j;
      this.k = k;
    }

    public double Norm()
    {
      return Math.Sqrt(r * r + i * i + j * j + k * k);
    }

    public static Quaternion operator -(Quaternion q)
    {
      Contract.Requires<ArgumentNullException>(q != null);

      return new Quaternion(-q.r, -q.i, -q.j, -q.k);
    }

    public Quaternion Conjugate()
    {
      return new Quaternion(r, -i, -j, -k);
    }

    // implicit conversion takes care of real*quaternion and real+quaternion
    public static implicit operator Quaternion(double d)
    {
      return new Quaternion(d, 0, 0, 0);
    }

    public static Quaternion operator +(Quaternion q1, Quaternion q2)
    {
      Contract.Requires<ArgumentNullException>(q1 != null);
      Contract.Requires<ArgumentNullException>(q2 != null);

      return new Quaternion(q1.r + q2.r, q1.i + q2.i, q1.j + q2.j, q1.k + q2.k);
    }

    public static Quaternion operator *(Quaternion q1, Quaternion q2)
    {
      Contract.Requires<ArgumentNullException>(q1 != null);
      Contract.Requires<ArgumentNullException>(q2 != null);

      return new Quaternion(
          q1.r * q2.r - q1.i * q2.i - q1.j * q2.j - q1.k * q2.k,
          q1.r * q2.i + q1.i * q2.r + q1.j * q2.k - q1.k * q2.j,
          q1.r * q2.j - q1.i * q2.k + q1.j * q2.r + q1.k * q2.i,
          q1.r * q2.k + q1.i * q2.j - q1.j * q2.i + q1.k * q2.r);
    }

    public static bool operator ==(Quaternion q1, Quaternion q2)
    {
      Contract.Requires<ArgumentNullException>(q1 != null);
      Contract.Requires<ArgumentNullException>(q2 != null);

      return q1.r == q2.r && q1.i == q2.i && q1.j == q2.j && q1.k == q2.k;
    }

    public static bool operator !=(Quaternion q1, Quaternion q2)
    {
      Contract.Requires<ArgumentNullException>(q1 != null);
      Contract.Requires<ArgumentNullException>(q2 != null);
      Contract.Ensures(Contract.Result<bool>() == (q1.r != q2.r || q1.i != q2.i || q1.j != q2.j || q1.k != q2.k));

      return !(q1 == q2);
    }

    #region Object Members

    public override bool Equals(object obj)
    {
      if (obj is Quaternion)
        return Equals((Quaternion)obj);

      return false;
    }

    public override int GetHashCode()
    {
      return r.GetHashCode() ^ i.GetHashCode() ^ j.GetHashCode() ^ k.GetHashCode();
    }

    public override string ToString()
    {
      return string.Format("Q({0}, {1}, {2}, {3})", r, i, j, k);
    }

    #endregion

    #region IEquatable<Quaternion> Members

    public bool Equals(Quaternion other)
    {
      Contract.Requires<ArgumentNullException>(other != null);

      return other == this;
    }

    #endregion
  }
}

/*
 Demonstration:
using System;
 
static class Program
{
    static void Main(string[] args)
    {
        Quaternion q = new Quaternion(1, 2, 3, 4);
        Quaternion q1 = new Quaternion(2, 3, 4, 5);
        Quaternion q2 = new Quaternion(3, 4, 5, 6);
        double r = 7;
 
        Console.WriteLine("q = {0}", q);
        Console.WriteLine("q1 = {0}", q1);
        Console.WriteLine("q2 = {0}", q2);
        Console.WriteLine("r = {0}", r);
 
        Console.WriteLine("q.Norm() = {0}", q.Norm());
        Console.WriteLine("q1.Norm() = {0}", q1.Norm());
        Console.WriteLine("q2.Norm() = {0}", q2.Norm());
 
        Console.WriteLine("-q = {0}", -q);
        Console.WriteLine("q.Conjugate() = {0}", q.Conjugate());
 
        Console.WriteLine("q + r = {0}", q + r);
        Console.WriteLine("q1 + q2 = {0}", q1 + q2);
        Console.WriteLine("q2 + q1 = {0}", q2 + q1);
 
        Console.WriteLine("q * r = {0}", q * r);
        Console.WriteLine("q1 * q2 = {0}", q1 * q2);
        Console.WriteLine("q2 * q1 = {0}", q2 * q1);
 
        Console.WriteLine("q1*q2 {0} q2*q1", (q1 * q2) == (q2 * q1) ? "==" : "!=");
    }
}
Output:
q = Q(1, 2, 3, 4)
q1 = Q(2, 3, 4, 5)
q2 = Q(3, 4, 5, 6)
r = 7
q.Norm() = 5.47722557505166
q1.Norm() = 7.34846922834953
q2.Norm() = 9.2736184954957
-q = Q(-1, -2, -3, -4)
q.Conjugate() = Q(1, -2, -3, -4)
q + r = Q(8, 2, 3, 4)
q1 + q2 = Q(5, 7, 9, 11)
q2 + q1 = Q(5, 7, 9, 11)
q * r = Q(7, 14, 21, 28)
q1 * q2 = Q(-56, 16, 24, 26)
q2 * q1 = Q(-56, 18, 20, 28)
q1*q2 != q2*q1
 */