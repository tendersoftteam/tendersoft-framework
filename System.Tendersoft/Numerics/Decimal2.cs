﻿using System.Diagnostics.Contracts;

namespace System.Tendersoft.Numerics
{
  public static class Decimal2
  {
    public static decimal? TryParse(string s)
    {
      return Decimal2.TryParse(s, System.Globalization.CultureInfo.CurrentUICulture);
    }

    public static decimal? TryParse(string s, System.Globalization.CultureInfo ci)
    {
      decimal result;
      if (decimal.TryParse(s, System.Globalization.NumberStyles.Number, ci, out result))
      {
        return result;
      }
      else
      {
        return (decimal?)null;
      }
    }

    private static decimal GetScaledOne(decimal value)
    {
      Contract.Requires<ArgumentException>(decimal.GetBits(value).Length >= 4);

      int[] bits = decimal.GetBits(value);
      // Generate a value +1, scaled using the same scaling factor as the input value
      bits[0] = 1;
      bits[1] = 0;
      bits[2] = 0;
      bits[3] = bits[3] & 0x00FF0000;
      return new decimal(bits);
    }

    public static decimal IncrementLastDigit(decimal value)
    {
      Contract.Ensures(value < 0 ? Contract.Result<decimal>() < value : Contract.Result<decimal>() > value);
      Contract.Ensures(Contract.Result<decimal>() != value);

      return value + (value < 0 ? -GetScaledOne(value) : GetScaledOne(value));
    }

    public static decimal DecrementLastDigit(decimal value)
    {
      Contract.Ensures(value < 0 ? Contract.Result<decimal>() > value : Contract.Result<decimal>() < value);
      Contract.Ensures(Contract.Result<decimal>() != value);

      return value + (value < 0 ? GetScaledOne(value) : -GetScaledOne(value));
    }

    public static int GetPrecision(this decimal value, bool ignoreTrailingZeros = false)
    {
      Contract.Ensures(Contract.Result<int>() >= 0);

      var argument = ignoreTrailingZeros ? (decimal)(double)value : value;
      return BitConverter.GetBytes(decimal.GetBits(argument)[3])[2];
    }
  }
}