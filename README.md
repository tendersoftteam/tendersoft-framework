# README #

### What is this repository for? ###

Repository contains few solutions with various helper functions and libraries. It was developed over the years and there is no plan to extend it much beyond its current scope. However, if you feel you can add or improve on what is already built then by all means go for it.

### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
There are few NUnit tests. New tests should be developed in xUnit though it is not mandatory - just a recommendation.
* Code review
* Other guidelines

formatting: 2 spaces, no tabs, regardless of language.

I sometimes use CodeMaid with mostly default settings.

### Who do I talk to? ###

* Repo owner or admin

bankowski { at } tendersoft [dot] pl