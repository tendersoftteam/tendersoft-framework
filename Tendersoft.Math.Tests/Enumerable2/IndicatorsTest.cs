// <copyright file="IndicatorsTest.cs" company="...">Copyright © ... 2006</copyright>
using System;
using System.Collections.Generic;
using Tendersoft.Enumerable2;
using Microsoft.Pex.Framework;
using Microsoft.Pex.Framework.Validation;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Tendersoft.Enumerable2
{
    /// <summary>This class contains parameterized unit tests for Indicators</summary>
    [PexClass(typeof(Indicators))]
    [PexAllowedExceptionFromTypeUnderTest(typeof(InvalidOperationException))]
    [PexAllowedExceptionFromTypeUnderTest(typeof(ArgumentException), AcceptExceptionSubtypes = true)]
    [TestClass]
    public partial class IndicatorsTest
    {
        /// <summary>Test stub for Ema(IEnumerable`1&lt;Double&gt;, Int32)</summary>
        [PexMethod]
        public IEnumerable<double?> Ema(IEnumerable<double> set, int period)
        {
            IEnumerable<double?> result = Indicators.Ema(set, period);
            return result;
            // TODO: add assertions to method IndicatorsTest.Ema(IEnumerable`1<Double>, Int32)
        }
    }
}
