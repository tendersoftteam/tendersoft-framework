﻿using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;
using System.Diagnostics.Contracts;

namespace System.Windows.Forms.Tendersoft
{
  public static class HtmlDocumentExtensions
  {
    //public static  XElement GetElementByAttribute(this HtmlDocument htmlDocument, XAttribute attribute)
    //{
    //    return htmlDocument.GetElementByAttribute(attribute.Name, attribute.Value);
    //}
    //public static XElement GetElementByAttribute(this HtmlDocument htmlDocument, XName name, string value)
    //{
    //    return htmlDocument.All.OfType<HtmlElement>()
    //        .Where(x => x.DomElement);
    //}
    public static IEnumerable<HtmlElement> GetElementsByAttribute(this HtmlElementCollection elementCollection, params XAttribute[] attributes)
    {
      Contract.Requires<ArgumentNullException>(elementCollection != null);
      Contract.Requires<ArgumentNullException>(attributes != null);
      Contract.Requires<ArgumentNullException>(Contract.ForAll(attributes, a => a != null));

      var attributes2 = attributes
          .Select(att => att.Name == "class" ? new XAttribute("className", att.Value) : att)
          .ToArray();

      var elements = elementCollection
          .OfType<HtmlElement>()
          .Where(x =>
               attributes2.All(att => x.GetAttribute(att.Name.LocalName).Equals(att.Value, StringComparison.InvariantCultureIgnoreCase))
          );
      return elements;
    }

    public static IEnumerable<HtmlElement> GetElementsByAttribute(this HtmlDocument document, params XAttribute[] attributes)
    {
      Contract.Requires<ArgumentNullException>(document != null);
      Contract.Requires<ArgumentNullException>(document.All != null);
      Contract.Requires<ArgumentNullException>(attributes != null);
      Contract.Requires<ArgumentNullException>(Contract.ForAll(attributes, a => a != null));

      return document.All.GetElementsByAttribute(attributes);

      //var elements0 = document.All
      //    .OfType<HtmlElement>()
      //    .Select(x => new { HtmlElement = x, XElement = XElement.Parse(x.OuterHtml) })
      //    .Where(x =>
      //         attributes.All(att =>
      //             x.XElement.Attributes().Any(iAttr =>
      //                 iAttr.Name.LocalName.Equals(att.Name.LocalName, StringComparison.InvariantCultureIgnoreCase)
      //                 && iAttr.Value.Equals(att.Value, StringComparison.InvariantCultureIgnoreCase)
      //             )
      //         )
      //     )
      //     .Select(x => x.HtmlElement);

      //var a00 = attributes2[0];
      //var a0 = document.All.OfType<HtmlElement>()
      //    .Where(x => x.GetAttribute(a00.Name.LocalName).Equals(a00.Value, StringComparison.InvariantCultureIgnoreCase)).ToList();
      //var a01 = attributes2[1];
      //var a1 = a0//document.All.OfType<HtmlElement>()
      //.Where(x => x.GetAttribute(a01.Name.LocalName).Equals(a01.Value, StringComparison.InvariantCultureIgnoreCase)).ToList();
      //var a02 = attribute[2];
      //var a2 = document.All.OfType<HtmlElement>()
      //    .Where(x => x.GetAttribute(a02.Name.LocalName) == a02.Value);
      //foreach (var element in elements)
      //{
      //    foreach (var attr in attribute)
      //    {
      //        var eAttr = element.GetAttribute(attr.Name.LocalName);
      //        if (eAttr != attr.Value) break;
      //    }
      //}
      //return elements;
    }
  }
}