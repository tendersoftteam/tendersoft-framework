﻿using System.ComponentModel;
using System.Diagnostics.Contracts;
using System.Text;
using System.Xml.Linq;
using Corsis.Xhtml;

namespace System.Windows.Forms.Tendersoft
{
  public partial class WebBrowserUserControl : UserControl
  {
    private bool blockPopupWindows = false;
    private WebBrowserController controller;

    private WebBrowserChangeNotifier notifier;

    public WebBrowserUserControl()
    {
      InitializeComponent();
      this.controller = new WebBrowserController(this.webBrowser1, this.urlTextBox, this.debugTextBox);
      this.webBrowser1.DocumentCompleted += (s, e) => { OnWebBrowserDocumentCompleted(e); };
      this.webBrowser1.CanGoBackChanged += (s, e) => this.backButton.Enabled = this.webBrowser1.CanGoBack;
      this.webBrowser1.CanGoForwardChanged += (s, e) => this.forwardButton.Enabled = this.webBrowser1.CanGoForward;
      this.innerSplitContainer.Panel2Collapsed = true;
      this.debugButton.Click += (s, e) => this.innerSplitContainer.Panel2Collapsed = !this.innerSplitContainer.Panel2Collapsed;
    }

    public event EventHandler<WebBrowserDocumentChangedEventArgs> WebBrowserDocumentChanged;

    public event EventHandler<WebBrowserDocumentCompletedEventArgs> WebBrowserDocumentCompleted;

    // = new WebBrowserChangeNotifier();

    [DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
    public bool BlockPopupWindows
    {
      get
      {
        return this.blockPopupWindows;
      }
      set
      {
        if (value)
        {
          this.webBrowser1.NewWindow += (s, e) => BlockPopupsAction(e);
        }
        else
        {
          this.webBrowser1.NewWindow -= (s, e) => BlockPopupsAction(e);
        }
        this.blockPopupWindows = value;
      }
    }

    [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
    public WebBrowser Browser { get { return this.webBrowser1; } }

    [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
    public XDocument Document
    {
      get
      {
        string documentString;

        var enc = Encoding.GetEncoding(this.webBrowser1.Document.DefaultEncoding);
        // what about thread safety?
        using (var sr = new System.IO.StreamReader(this.webBrowser1.DocumentStream, enc))
        {
          documentString = sr.ReadToEnd();
        }

        var xdoc = Html2Xhtml.RunAsFilter(sw => sw.Write(documentString))
            .ReadToXDocument(keepXhtmlNamespace: true);
        return xdoc;
      }
    }

    [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
    public HtmlDocument DocumentHtml { get { return this.webBrowser1.Document; } }

    [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
    public object DomDocument { get { return this.webBrowser1.Document.DomDocument; } }

    [DesignerSerializationVisibility(DesignerSerializationVisibility.Visible)]
    public bool SuppressScriptErrors
    {
      get { return this.webBrowser1.ScriptErrorsSuppressed; }
      set { this.webBrowser1.ScriptErrorsSuppressed = value; }
    }

    [DesignerSerializationVisibility(DesignerSerializationVisibility.Hidden)]
    public Uri Url { get { return this.webBrowser1.Url; } }

    //[Obsolete("Will be removed shortly.")]
    public void Debug(string msg)
    {
      controller.SendToDebugWindow(msg);
    }

    public void Navigate(Uri address)
    {
      Contract.Requires<ArgumentNullException>(address != null);

      this.urlTextBox.Text = address.ToString();
      controller.GoNavigate();
    }

    internal virtual void OnWebBrowserDocumentChanged(WebBrowserDocumentChangedEventArgs e)
    {
      controller.OnDocumentChanged(this, e);

      var temp = WebBrowserDocumentChanged;
      if (temp != null)
      {
        temp(this, e);
      }
    }

    internal virtual void OnWebBrowserDocumentCompleted(WebBrowserDocumentCompletedEventArgs e)
    {
      controller.OnDocumentCompleted(this, e);
      // register DocumentChanged monitor
      notifier = new WebBrowserChangeNotifier();
      notifier.RegisterNotifier(this);

      var temp = WebBrowserDocumentCompleted;
      if (temp != null)
      {
        temp(this, e);
      }
    }

    private void backButton_Click(object sender, EventArgs e)
    {
      controller.GoBackward();
    }

    private void BlockPopupsAction(CancelEventArgs e)
    {
      e.Cancel = true;
    }

    //internal void WebBrowserUserControl_WebBrowserDocumentChanged(object sender, WebBrowserDocumentChangedEventArgs e)
    //{
    //    controller.OnDocumentChanged(sender, e, WebBrowserDocumentChanged);
    //}

    private void forwardButton_Click(object sender, EventArgs e)
    {
      controller.GoForward();
    }

    private void goButton_Click(object sender, EventArgs e)
    {
      controller.GoNavigate();
    }

    private void refreshButton_Click(object sender, EventArgs e)
    {
      controller.RefreshPage();
    }

    private void urlTextBox_KeyDown(object sender, KeyEventArgs e)
    {
      controller.GoNavigate(e);
    }

    private void WebBrowserUserControl_KeyDown(object sender, KeyEventArgs e)
    {
      controller.RefreshPage(e);
    }
  }
}