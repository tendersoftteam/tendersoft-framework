﻿using System.Diagnostics.Contracts;
using System.Threading;
using System.Threading.Tasks;
using System.Threading.Tasks.Tendersoft;

namespace System.Windows.Forms.Tendersoft
{
  public static class FormExtensions
  {
    [Obsolete]
    public static Task RunOnStaThread(this Form form)
    {
      Contract.Requires<ArgumentNullException>(form != null);

      return form.RunOnStaThread(SynchronizationContext.Current);
    }

    [Obsolete]
    public static Task RunOnStaThread(this Form form, SynchronizationContext context)
    {
      Contract.Requires(form != null);

      Action action = () =>
      {
        System.Windows.Forms.Application.EnableVisualStyles();
        System.Windows.Forms.Application.SetCompatibleTextRenderingDefault(false);

        Contract.Assert(System.Windows.Forms.WindowsFormsSynchronizationContext.Current == context, "StaThread => bad contexts");
        System.Windows.Forms.Application.Run(form);
#if TRACE
        System.Diagnostics.Trace.WriteLine("Form was run");
#endif
      };
      var staChartFromTask = TaskEx.RunStaTask(action);
      return staChartFromTask;
    }

    [Obsolete]
    public static Task RunOnStaThread<T>(this T cancellableForm, CancellationToken ct) where T : System.Windows.Forms.Form, ICancellableForm
    {
      Contract.Requires<ArgumentNullException>(cancellableForm != null);
      Contract.Requires<ArgumentNullException>(ct != null);

      return cancellableForm.RunOnStaThread(ct, SynchronizationContext.Current);
    }

    [Obsolete]
    public static Task RunOnStaThread<T>(this T cancellableForm, CancellationToken ct, SynchronizationContext context) where T : System.Windows.Forms.Form, ICancellableForm
    {
      Contract.Requires(cancellableForm != null);
      Contract.Requires(ct != null);

      ct.Register(() => cancellableForm.InvokeOnUIThreadAsync(() => cancellableForm.Cancel()));

      var staChartFromTask = cancellableForm.RunOnStaThread();
      return staChartFromTask;
    }
  }
}