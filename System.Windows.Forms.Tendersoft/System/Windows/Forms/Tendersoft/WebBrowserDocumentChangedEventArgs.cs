﻿using System.Diagnostics.Contracts;
using System.Xml.Linq;

namespace System.Windows.Forms.Tendersoft
{
  public class WebBrowserDocumentChangedEventArgs : EventArgs
  {
    public WebBrowserDocumentChangedEventArgs(HtmlDocument document)
    {
      Contract.Requires(document != null, "HtmlDocument cannot be null as its Body element will be queried.");
      // unfortunately cloning through Clone<HtmlDocument>() throws a SerializationException as the HtmlDocument is not marked with SerializableAttribute
      this.BodyElement = new XElement("body", document.Body != null ? document.Body.InnerHtml : string.Empty);//.Clone<HtmlDocument>();
    }

    public XElement BodyElement
    {
      get;
      private set;
    }
  }
}