﻿using System.Diagnostics.Contracts;
using System.Text;

namespace System.Windows.Forms.Tendersoft
{
  internal class WebBrowserController
  {
    private WebBrowser webBrowser;
    private TextBox urlTextBox;
    private TextBox debugTextBox;

    public WebBrowserController(WebBrowser webBrowser, TextBox urlTextBox, TextBox debugTextBox)
    {
      Contract.Requires(webBrowser != null, "WebBrowser is null");
      Contract.Requires(urlTextBox != null, "urlTextBox is null");
      Contract.Requires(debugTextBox != null, "debugTextBox is null");
      this.webBrowser = webBrowser;
      this.urlTextBox = urlTextBox;
      this.debugTextBox = debugTextBox;
    }

    internal void GoNavigate()
    {
      var msg = string.Format("Navigating: {0}\n", this.urlTextBox.Text);
      this.SendToDebugWindow(msg);
      this.webBrowser.Navigate(this.urlTextBox.Text);
    }

    internal void UpdateUrl(Uri url)
    {
      this.urlTextBox.Text = url.ToString();
    }

    internal void RefreshPage(KeyEventArgs e)
    {
      if ((e.KeyCode == Keys.R && e.Control) || e.KeyCode == Keys.F5)
      {
        RefreshPage();
      }
    }

    internal void RefreshPage()
    {
      this.webBrowser.Refresh(WebBrowserRefreshOption.Completely);
    }

    internal void GoForward()
    {
      if (this.webBrowser.CanGoForward)
      {
        this.webBrowser.GoForward();
      }
    }

    internal void GoBackward()
    {
      if (this.webBrowser.CanGoBack)
      {
        this.webBrowser.GoBack();
      }
    }

    internal void GoNavigate(KeyEventArgs e)
    {
      if (e.KeyCode == Keys.Enter)
      {
        GoNavigate();
      }
    }

    internal void OnDocumentCompleted(object sender, WebBrowserDocumentCompletedEventArgs e)
    {
      //this.UpdateUrl(e.Url);
      var sb = new StringBuilder();
      sb.AppendLine(DateTime.Now + " Completed: " + e.Url.ToString() + "\n");
      this.SendToDebugWindow(sb.ToString());
    }

    internal void OnDocumentChanged(object sender, WebBrowserDocumentChangedEventArgs e)
    {
      //this.UpdateUrl(this.webBrowser.Url);
      var sb = new StringBuilder();
      sb.AppendLine(string.Format("{2} Changed: {0}, len: {1}\n", this.webBrowser.Url.ToString(), e.BodyElement.ToString().Length, DateTime.Now));
      this.SendToDebugWindow(sb.ToString());
    }

    internal void SendToDebugWindow(string debugMsg)
    {
      this.debugTextBox.AppendText(debugMsg);
      var maxLength = 5000;
      if (this.debugTextBox.Text.Length >= maxLength)
      {
        this.debugTextBox.Text = this.debugTextBox.Text.Substring(this.debugTextBox.Text.Length - maxLength);
      }
    }
  }
}