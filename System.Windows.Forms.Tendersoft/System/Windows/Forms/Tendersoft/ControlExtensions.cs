﻿using System.Diagnostics.Contracts;

namespace System.Windows.Forms.Tendersoft
{
  public static class ControlExtensions
  {
    public static object Invoke(this Control control, Action action)
    {
      Contract.Requires(control != null);
      Contract.Requires(action != null);

      return control.Invoke(
          new MethodInvoker(action)
      );
    }

    public static IAsyncResult InvokeOnUIThreadAsync(this Control control, Action code)
    {
      Contract.Requires(control != null);
      Contract.Requires(code != null);

      if (!control.IsHandleCreated) { var handle = control.Handle; }
      if (control.InvokeRequired)
      {
        return control.BeginInvoke(code);
      }
      else
      {
        Contract.Assert(!control.InvokeRequired && control.IsHandleCreated);
        return code.BeginInvoke((x) => code(), null);
      }
    }

    public static T InvokeOnUIThread<T>(this Control control, Func<T> code)
    {
      Contract.Requires(control != null);
      Contract.Requires(code != null);

      if (!control.IsHandleCreated) { var handle = control.Handle; }
      if (control.InvokeRequired)
      {
        return (T)control.Invoke(code);
      }
      else
      {
        Contract.Assert(!control.InvokeRequired && control.IsHandleCreated);
        return code.Invoke();
      }
    }
  }
}