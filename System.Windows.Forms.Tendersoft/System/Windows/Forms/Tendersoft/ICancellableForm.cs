﻿namespace System.Windows.Forms.Tendersoft
{
  public interface ICancellableForm
  {
    void Cancel();
  }
}