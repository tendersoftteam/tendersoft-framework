﻿using mshtml;

namespace System.Windows.Forms.Tendersoft
{
  public class HTMLChangeSink : IHTMLChangeSink
  {
    private System.Windows.Forms.Tendersoft.WebBrowserUserControl webBrowserUC;

    public HTMLChangeSink(System.Windows.Forms.Tendersoft.WebBrowserUserControl webBrowserUC)
    {
      // TODO: Complete member initialization
      this.webBrowserUC = webBrowserUC;
    }

    #region IHTMLChangeSink Members

    public void Notify()
    {// fire WebBrowserDocumentChanged with document content as EventArg
      var changedDocument = webBrowserUC.DocumentHtml;//.Document;

      webBrowserUC.Debug("Notify: " + changedDocument.Url.AbsoluteUri + "\n");

      var eventArg = new WebBrowserDocumentChangedEventArgs(changedDocument);
      webBrowserUC.OnWebBrowserDocumentChanged(eventArg);
    }

    #endregion IHTMLChangeSink Members
  }
}