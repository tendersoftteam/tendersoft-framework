﻿using mshtml;

namespace System.Windows.Forms.Tendersoft
{
  internal class WebBrowserChangeNotifier
  {
    private IHTMLChangeLog changeLog = null;
    private IMarkupContainer2 container2 = null;
    private IHTMLChangeSink changeSync = null;
    private uint m_cookie;

    public void RegisterNotifier(WebBrowserUserControl webBrowserUC)
    {
      container2 = null;
      changeLog = null;
      changeSync = new HTMLChangeSink(webBrowserUC);
      // Get a handle to the current MarkupContainer
      container2 = (IMarkupContainer2)(webBrowserUC.DomDocument);

      // ' Create the change log
      //container2.CreateChangeLog(changeSync, out changeLog, 1, 1);
      container2.RegisterForDirtyRange(changeSync, out m_cookie);
    }
  }
}