﻿// -----------------------------------------------------------------------
// <copyright file="ChartingExtensions.cs" company="">
// TODO: Update copyright text.
// </copyright>
// -----------------------------------------------------------------------

namespace System.Windows.Forms.DataVisualization.Charting.Tendersoft
{
  using System;
  using System.Collections.Generic;
  using System.Linq;
  using System.Text;
  using System.Windows.Forms.DataVisualization.Charting;
  using System.Diagnostics.Contracts;

  /// <summary>
  /// TODO: Update summary.
  /// </summary>
  public static class ChartingExtensions
  {
    public static Series ToSeries(this IEnumerable<double> sequence, string name, SeriesChartType seriesType = SeriesChartType.FastPoint)
    {
      Contract.Requires<ArgumentNullException>(sequence != null);

      var series = new Series(name);
      series.ChartType = seriesType;
      series.Points.DataBindY(sequence.ToArray());

      return series;
    }

    public static Series ToSeries(this IEnumerable<double[]> sequence, string name, SeriesChartType seriesType = SeriesChartType.FastPoint, bool use0IndexAsX = false)
    {
      Contract.Requires<ArgumentNullException>(sequence != null);
      Contract.Requires<ArgumentException>(Contract.ForAll(sequence, s => s != null));
      Contract.Requires<ArgumentException>(Contract.ForAll(sequence, s => s.Length >= 2));

      var series = new Series(name);
      series.ChartType = seriesType;
      if (use0IndexAsX)
      {
        var xs = sequence.Select(x => x[0]).ToArray();
        var ys = sequence
            .Select(x => x[1]).ToArray();
        //series.YValuesPerPoint = ys.First().Length;
        series.Points.DataBindXY(xs, ys);
      }
      else
      {
        series.Points.DataBindY(sequence.ToArray());
      }

      return series;
    }
  }
}
