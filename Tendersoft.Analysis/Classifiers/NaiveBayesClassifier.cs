﻿// -----------------------------------------------------------------------
// <copyright file="NaiveBayesClassifier.cs" company="">
// LGPL v3.0 license
// </copyright>
// -----------------------------------------------------------------------

namespace Tendersoft.Analysis.Classifiers
{
  using System;
  using System.Collections.Generic;
  using System.Diagnostics.Contracts;
  using System.Linq;
  using System.Linq.Expressions;
  using System.Reflection;
  using System.Reflection.Tendersoft;
  using System.Runtime.Serialization;

  public class NaiveBayesClassifier<T> where T : class, new()
  {
    private List<T> _trainingData;
    private List<PropertyInfo> _entityClasses;
    //private DataDefinition<TestClass> dataDefinition;
    //private IEnumerable<TestClass> trainingData;

    public NaiveBayesClassifier()
      : this(null, Enumerable.Empty<T>())
    {
    }

    public NaiveBayesClassifier(IEnumerable<T> trainingData)
      : this(null, trainingData)
    {
    }

    public NaiveBayesClassifier(DataDefinition<T> dataDefinition, IEnumerable<T> trainingData)
    {
      _trainingData = new List<T>();
      this._trainingData.AddRange(trainingData);

      _entityClasses = typeof(T)
          .GetProperties()
          .Where(p =>
            !p.GetCustomAttributes(typeof(IgnoreDataMemberAttribute), true)
              .Any()
          )
          .ToList();

      if (dataDefinition != null)
      {
        var defaultEntity = new T();
        var propNames = dataDefinition.ClassesDefinitions
          .Select(cd => cd.Key)
          .Select(e => defaultEntity.GetMemberName(e));

        _entityClasses = _entityClasses
          .Where(p => propNames.Contains(p.Name))
          .ToList();
      }
    }

    public double[] Classify<TResult>(T entity, Expression<Func<T, TResult>> expression, Func<T, bool> classValueFunc)
    {
      var positiveResult = CalculateClassProbability(entity, expression, classValueFunc);
      var negativeResult = CalculateClassProbability(entity, expression, Negate(classValueFunc));
      return new double[] { positiveResult, negativeResult };
    }

    public double CalculateClassProbability<TResult>(T entity, Expression<Func<T, TResult>> expression, Func<T, bool> classValueFunc)
    {
      var conditioningEventProperty = entity.GetMemberPropertyInfo(expression);
      var conditionedEventProperties = _entityClasses.Except(new PropertyInfo[] { conditioningEventProperty });

      var probabilities = conditionedEventProperties.Select(prop => ConditionalProbability(entity, prop, classValueFunc));

      // p0 * p1 * p2 * p3; // Risky if any very small values
      //var numerator = probabilities.Aggregate((accumultor, x) => accumultor * x);
      // use Exp(Log(p0)+Log(p1)+...+Log(pn)) instead
      //return Math.Exp(Math.Log(p0) + Math.Log(p1) + Math.Log(p2) + Math.Log(p3));
      var numerator = Math.Exp(probabilities.Aggregate(0.0, (accumultor, x) => accumultor + Math.Log(x)));
      var denominator = 1.0;

      return numerator / denominator;
    }

    private static Func<T, bool> Negate(Func<T, bool> predicate)
    {
      return x => !predicate(x);
    }

    private double ConditionalProbability(T entity, PropertyInfo conditionedEventProperty, Func<T, bool> conditioningEventAttribute, bool isLaplacianSmoothing = true)
    {
      //var entityValue = conditioningEventAttribute(entity);
      var numerator = _trainingData
        .Count(x =>
         object.Equals(conditionedEventProperty.GetValue(x, null), conditionedEventProperty.GetValue(entity, null))
          && conditioningEventAttribute(x)
        );
      var denominator = _trainingData.Count(x => conditioningEventAttribute(x));

      if (isLaplacianSmoothing)
      {
        numerator += 1;

        var countsSmoothing = _entityClasses.Count - 1;
        Contract.Assert(countsSmoothing >= 1);

        denominator += countsSmoothing;
      }

      return numerator / denominator;
    }

    private double ConditionalProbability2<TResult>(T entity, PropertyInfo conditionedEventProperty, Func<T, TResult> conditioningEventAttribute, bool isLaplacianSmoothing = true)
    {
      var entityValue = conditioningEventAttribute(entity);
      var numerator = _trainingData
        .Count(x =>
         object.Equals(conditionedEventProperty.GetValue(x, null), conditionedEventProperty.GetValue(entity, null))
          && object.Equals(entityValue, conditioningEventAttribute(x))
        );
      var denominator = _trainingData.Count(x => object.Equals(entityValue, conditioningEventAttribute(x)));

      if (isLaplacianSmoothing)
      {
        numerator += 1;

        var countsSmoothing = entity.GetType()
          .GetProperties()
          .Count(p =>
            !p.GetCustomAttributes(typeof(IgnoreDataMemberAttribute), true)
              .Any()
          ) - 1;
        Contract.Assert(countsSmoothing >= 1);

        denominator += countsSmoothing;
      }

      return numerator / denominator;
    }
  }
}