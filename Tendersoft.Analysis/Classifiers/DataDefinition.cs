﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace Tendersoft.Analysis.Classifiers
{
  public class DataDefinition<T>
  {
    public DataDefinition()
    {
      ClassesDefinitions = new Dictionary<Expression<Func<T, object>>, ClassDefinition<T>>();
    }

    public Dictionary<Expression<Func<T, object>>, ClassDefinition<T>> ClassesDefinitions { get; set; }
  }
}