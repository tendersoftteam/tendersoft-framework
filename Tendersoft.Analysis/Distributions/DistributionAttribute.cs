﻿// -----------------------------------------------------------------------
// <copyright file="DistributionAttribute.cs" company="">
// TODO: Update copyright text.
// </copyright>
// -----------------------------------------------------------------------

namespace Tendersoft.Analysis.Distributions
{
  using System;

  /// <summary>
  /// TODO: Update summary.
  /// </summary>
  [AttributeUsage(AttributeTargets.Property)]
  public class DistributionAttribute : Attribute
  {
    public Accord.Statistics.Distributions.IDistribution Type { get; set; }

    // public Accord.Statistics.Distributions.IFittableDistribution GenProp { get; set; }
  }
}