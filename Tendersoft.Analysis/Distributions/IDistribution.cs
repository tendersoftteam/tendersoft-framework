﻿namespace Tendersoft.Analysis.Distributions
{
  public interface IDistribution
  {
    double Evaluate(double x);
  }
}