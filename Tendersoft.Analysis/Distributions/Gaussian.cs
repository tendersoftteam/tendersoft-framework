﻿// -----------------------------------------------------------------------
// <copyright file="Gaussian.cs" company="">
// LGPL v3.0 license
// </copyright>
// -----------------------------------------------------------------------

namespace Tendersoft.Analysis.Distributions
{
  using System;

  /// <summary>
  /// TODO: Update summary.
  /// </summary>
  public class Gaussian : IDistribution
  {
    private double _stdDev;
    private double _mean;

    /// <summary>
    /// Creates Guassian/Normal distribution
    /// </summary>
    /// <param name="mean">mi - mean</param>
    /// <param name="stdDev">sigma - standard deviation</param>
    public Gaussian(double mean, double stdDev)
    {
      _stdDev = stdDev;
      _mean = mean;
    }

    public double Evaluate(double x)
    {
      return
        Math.Exp(Math.Pow(x - _mean, 2) / (-2 * _stdDev * _stdDev))
        / (Math.Sqrt(2 * Math.PI) * _stdDev);
    }
  }
}