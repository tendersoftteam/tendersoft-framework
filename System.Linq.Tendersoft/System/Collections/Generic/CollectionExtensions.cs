﻿using System.Diagnostics.Contracts;

namespace System.Collections.Generic.Tendersoft
{
  public static class CollectionExtensions
  {
    /// <summary>
    /// Tries to get a Value out of dictionary, returns Default value if no matching Key is found
    /// </summary>
    /// <typeparam name="K"></typeparam>
    /// <typeparam name="V"></typeparam>
    /// <param name="dictionary"></param>
    /// <param name="key"></param>
    /// <returns>Value or Default if no matching Key is found</returns>
    public static V TryGetValue<K, V>(this IDictionary<K, V> dictionary, K key)
    {
      Contract.Requires(dictionary != null, "null dictionary not expected by extension method");

      V value;
      var result = dictionary.TryGetValue(key, out value);
      return result ? value : default(V);
    }

    /// <summary>
    /// Tries to get a Value out of dictionary, returns Nullable of V equal null if no matching Key is found
    /// </summary>
    /// <typeparam name="K"></typeparam>
    /// <typeparam name="V">Non-nullable struct/value type</typeparam>
    /// <param name="dictionary"></param>
    /// <param name="key"></param>
    /// <returns></returns>
    public static V? TryGetNullabeValue<K, V>(this IDictionary<K, V> dictionary, K key) where V : struct
    {
      Contract.Requires(dictionary != null, "null dictionary not expected by extension method");

      V value;
      var result = dictionary.TryGetValue(key, out value);
      return result ? value : (Nullable<V>)null;
    }

    public static V GetForcedValue<K, V>(this IDictionary<K, V> dictionary, K key) where V : new()
    {
      Contract.Requires(dictionary != null, "null dictionary not expected by extension method");

      V value;
      var result = dictionary.TryGetValue(key, out value);
      return result ? value : new V();
    }

    /// <summary>
    /// Adds a new element. Updates the old value if Key already exists.
    /// </summary>
    /// <typeparam name="K"></typeparam>
    /// <typeparam name="V"></typeparam>
    /// <param name="dictionary"></param>
    /// <param name="key"></param>
    /// <param name="value"></param>
    /// <returns>True if element was updated, False if added</returns>
    public static bool AddOrUpdate<K, V>(this IDictionary<K, V> dictionary, K key, V value)
    {
      Contract.Requires(dictionary != null, "null dictionary not expected by extension method");

      var keyRemoved = dictionary.Remove(key);
      dictionary.Add(key, value);
      return keyRemoved;
    }

    /*
     * <at> author Vladimir Yaroslavskiy
     * <at> version 2009.09.10 m765
     * src: http://permalink.gmane.org/gmane.comp.java.openjdk.core-libs.devel/2628
     */

    #region DualPivotQuicksort

    public static void DualPivotQuicksort<T>(this IList<T> a) where T : IComparable<T>
    {
      Contract.Requires<ArgumentNullException>(a != null);

      DualPivotQuicksort(a, 0, a.Count - 1);
    }

    public static void DualPivotQuicksort<T>(this IList<T> a, int fromIndex, int toIndex) where T : IComparable<T>
    {
      Contract.Requires<ArgumentNullException>(a != null);
      Contract.Requires<IndexOutOfRangeException>(fromIndex >= 0);
      Contract.Requires<IndexOutOfRangeException>(toIndex < a.Count);
      //Contract.Requires<ArgumentException>(fromIndex <= toIndex);
      Contract.Requires<ArgumentException>(fromIndex < toIndex);

      DualPivotQuicksort(a, fromIndex, toIndex, 3);
    }

    private static void Swap<T>(IList<T> a, int i, int j)
    {
      Contract.Requires<ArgumentNullException>(a != null);
      Contract.Requires<ArgumentException>(a.Count >= 1);
      Contract.Requires<IndexOutOfRangeException>(i >= 0);
      Contract.Requires<IndexOutOfRangeException>(j >= 0);
      Contract.Requires<IndexOutOfRangeException>(i < a.Count);
      Contract.Requires<IndexOutOfRangeException>(j < a.Count);
      Contract.Ensures(Contract.Equals(Contract.OldValue(a[i]), a[j]));
      Contract.Ensures(Contract.Equals(Contract.OldValue(a[j]), a[i]));

      var temp = a[i];
      a[i] = a[j];
      a[j] = temp;
    }

    private static void DualPivotQuicksort<T>(IList<T> a, int left, int right, int div) where T : IComparable<T>
    {
      Contract.Requires<ArgumentNullException>(a != null);
      Contract.Requires<IndexOutOfRangeException>(left >= 0);
      Contract.Requires<IndexOutOfRangeException>(right < a.Count);
      //Contract.Requires<ArgumentException>(left <= right);
      Contract.Requires<ArgumentException>(left < right);
      Contract.Requires<ArgumentException>(div > 0);

      int len = right - left;

      if (len < 27)
      { // insertion sort for tiny array
        for (int i = left + 1; i <= right; i++)
        {
          for (int j = i; j > left && a[j].CompareTo(a[j - 1]) < 0; j--)
          {
            Swap(a, j, j - 1);
          }
        }
        return;
      }
      int third = len / div;

      // "medians"
      int m1 = left + third;
      int m2 = right - third;

      if (m1 <= left)
      {
        m1 = left + 1;
      }
      if (m2 >= right)
      {
        m2 = right - 1;
      }
      if (a[m1].CompareTo(a[m2]) < 0)
      {
        Swap(a, m1, left);
        Swap(a, m2, right);
      }
      else
      {
        Swap(a, m1, right);
        Swap(a, m2, left);
      }
      // pivots
      var pivot1 = a[left];
      var pivot2 = a[right];

      // pointers
      int less = left + 1;
      int great = right - 1;

      // sorting
      for (int k = less; k <= great; k++)
      {
        if (a[k].CompareTo(pivot1) < 0)
        {
          Swap(a, k, less++);
        }
        else if (a[k].CompareTo(pivot2) > 0)
        {
          while (k < great && a[great].CompareTo(pivot2) > 0)
          {
            great--;
          }
          Swap(a, k, great--);

          if (a[k].CompareTo(pivot1) < 0)
          {
            Swap(a, k, less++);
          }
        }
      }
      // swaps
      int dist = great - less;

      if (dist < 13)
      {
        div++;
      }
      Swap(a, less - 1, left);
      Swap(a, great + 1, right);

      // subarrays
      DualPivotQuicksort(a, left, less - 2, div);
      DualPivotQuicksort(a, great + 2, right, div);

      // equal elements
      if (dist > len - 13 && !object.Equals(pivot1, pivot2))
      {
        for (int k = less; k <= great; k++)
        {
          if (object.Equals(a[k], pivot1))
          {
            Swap(a, k, less++);
          }
          else if (object.Equals(a[k], pivot2))
          {
            Swap(a, k, great--);

            if (object.Equals(a[k], pivot1))
            {
              Swap(a, k, less++);
            }
          }
        }
      }
      // subarray
      if (pivot1.CompareTo(pivot2) < 0)
      {
        DualPivotQuicksort(a, less, great, div);
      }
    }

    #endregion DualPivotQuicksort
  }
}