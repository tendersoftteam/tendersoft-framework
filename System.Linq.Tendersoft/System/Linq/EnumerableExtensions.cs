﻿using System.Collections.Generic;
using System.Diagnostics.Contracts;
using System.Reactive.Linq;
using System.Reflection.Tendersoft;
using System.Tendersoft;
using System.Tendersoft.Numerics;

namespace System.Linq.Tendersoft
{
  public static class LinqExtensions
  {
    public static Dictionary<int, Tuple<decimal, decimal, int>> Bucketize(this IEnumerable<decimal> source, int totalBuckets)
    {
      Contract.Requires<ArgumentNullException>(source != null);
      Contract.Requires<ArgumentException>(totalBuckets > 0);

      var data = source.ToList();
      var min = data.Min();
      var max = data.Max();
      var bucketSize = (max - min) / totalBuckets;

      var bucketInfo = Enumerable.Range(0, totalBuckets)
        .Select(i => new { N = i, LeftLimit = min + i * bucketSize, RightLimit = Decimal2.DecrementLastDigit(min + (i + 1) * bucketSize) })
        .ToList();

      var histogram = new int[totalBuckets];
      foreach (var value in data)
      {
        var bucketIndex = (int)((value - min) / bucketSize);
        // can happen due to rounding
        if (bucketIndex >= totalBuckets)
        {
          bucketIndex = histogram.Length - 1;
        }

        histogram[bucketIndex]++;
      }

      return bucketInfo
        .Zip(histogram, (bi, h) => new { bi.N, bi.LeftLimit, bi.RightLimit, Value = h })
        .ToDictionary(x => x.N, x => Tuple.Create(x.LeftLimit, x.RightLimit, x.Value));
    }

    #region Cast

    public static IEnumerable<TResult> CastExplicit<T, TResult>(this IEnumerable<T> source)
    {
      Contract.Requires<ArgumentNullException>(source != null);

      var actual = source as IEnumerable<TResult>;
      if (actual != null)
        return actual;

      return CreateCastIterator<T, TResult>(source);
    }

    private static IEnumerable<TResult> CreateCastIterator<T, TResult>(IEnumerable<T> source)
    {
      Contract.Requires<ArgumentNullException>(source != null);

      return source.Select(x => x.CastTo<T, TResult>());
    }

    #endregion Cast

    public static T RandomItem<T>(this IEnumerable<T> items)
    {
      Contract.Requires<ArgumentNullException>(items != null);
      Contract.Requires(items.Count() >= 1);

      var itemsCol = items as IList<T>;
      if (itemsCol != null)
      {
        var randomIndex = RandomGen2.Next() % itemsCol.Count;
        return itemsCol[randomIndex];
      }
      return items.RandomSample(1).First();
    }

    /// <summary>
    /// Randomly selects N=sampeSize elements
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="items"></param>
    /// <param name="sampleSize"></param>
    /// <returns></returns>
    public static IEnumerable<T> RandomSample<T>(this IEnumerable<T> items, int sampleSize)
    {
      Contract.Requires<ArgumentNullException>(items != null);
      Contract.Requires(sampleSize >= 0);
      Contract.Ensures(Contract.Result<IEnumerable<T>>() != null);
      Contract.Ensures(Contract.Result<IEnumerable<T>>().Count() >= 0);
      Contract.Ensures(Contract.Result<IEnumerable<T>>().Count() <= items.Count());
      Contract.Ensures(Contract.Result<IEnumerable<T>>().Count() <= sampleSize);

      var randomSample = items
          .Randomize()
          .Take(sampleSize);

      return randomSample;
    }

    private static Random _r = new Random();

    public static IEnumerable<T> Randomize<T>(this IEnumerable<T> items)
    {
      Contract.Requires<ArgumentNullException>(items != null);
      Contract.Ensures(Contract.Result<IEnumerable<T>>() != null);
      Contract.Ensures(Contract.Result<IEnumerable<T>>().Count() == items.Count());

      var randomSample = items
          .Select(x => new { x, Rank = RandomGen2.NextDouble() })
          .OrderBy(x => x.Rank)
          .Select(y => y.x);

      return randomSample;
    }

    [Pure]
    public static IEnumerable<Tuple<T, V>> Product<T, V>(this IEnumerable<T> items, IEnumerable<V> items2)
    {
      Contract.Requires<ArgumentNullException>(items != null);
      Contract.Requires<ArgumentNullException>(items2 != null);

      foreach (var item in items)
      {
        foreach (var item2 in items2)
        {
          yield return Tuple.Create(item, item2);
        }
      }
    }

    /// <summary>
    /// Logs implicit notifications to console.
    /// </summary>
    /// <example>
    /// <code>myStream.Log().Subscribe(....);</code>
    /// </example>
    public static IObservable<T> Log<T>(this IObservable<T> stream)
    {
      Contract.Requires<ArgumentNullException>(stream != null);

      return stream.Materialize()
      .Do(Console.WriteLine)
      .Dematerialize();
    }

    [Pure]
    public static IEnumerable<T[]> SlidingWindow<T>(this IEnumerable<T> inputSequence, int windowLength)
    {
      Contract.Requires<ArgumentNullException>(inputSequence != null, "inputSequence is null");
      Contract.Requires<ArgumentException>(windowLength > 1, "windowLength is less than or equal 1");
      Contract.Requires<ArgumentException>(inputSequence.Count() >= windowLength, "sequence contains less than windowsLength elements");
      Contract.Ensures(Contract.Result<IEnumerable<T[]>>().Count() == inputSequence.Count() - windowLength + 1, "resulting sequence has invalid number of elements");
      Contract.Ensures(Contract.ForAll(Contract.Result<IEnumerable<T[]>>(), x => x.Count() == windowLength), "not all elements have the same windowLenght");

      List<T> listWindow = new List<T>(windowLength);

      foreach (var item in inputSequence)
      {
        listWindow.Add(item);
        if (listWindow.Count < windowLength)
        {
          continue;
        }
        if (listWindow.Count > windowLength)
        {
          listWindow.RemoveAt(0);
        }
        yield return listWindow.ToArray();
      }
    }

    /// <summary>
    /// Simple wrapper around GroupBy(x=>true/false).Orderby(x=>x.Key)
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="source"></param>
    /// <param name="partitionFunc"></param>
    /// <returns></returns>
    [Pure]
    public static IEnumerable<IEnumerable<T>> Partition<T>(this IEnumerable<T> source, Func<T, bool> partitionFunc)
    {
      Contract.Requires<ArgumentNullException>(source != null);
      Contract.Requires<ArgumentNullException>(partitionFunc != null);

      return source.GroupBy(partitionFunc)
        .OrderBy(x => x.Key);
    }

    /// <summary>
    /// Transposes a m*n matrix of T's into a n*m matrix of V's
    /// </summary>
    /// <typeparam name="T">input type</typeparam>
    /// <typeparam name="V">output type</typeparam>
    /// <param name="source">source matrix</param>
    /// <param name="elementSelector">T -> V transformation selector</param>
    /// <returns>Transposed matrix</returns>
    /// <exception cref="ArgumentNullException">Throws when source is null or one of source's elements is null</exception>
    /// <exception cref="ArgumentException">Throw when source is not m by n matrix</exception>
    /// <remarks>
    /// Source inspired by: http://stackoverflow.com/a/5039863
    /// </remarks>
    [Pure]
    public static IEnumerable<IEnumerable<V>> Transpose<T, V>(this IEnumerable<IEnumerable<T>> source, Func<T, V> elementSelector)
    {
      Contract.Requires<ArgumentNullException>(source != null, "source is null");
      Contract.Requires<ArgumentNullException>(elementSelector != null, "elementSelector is null");
      Contract.Requires<ArgumentNullException>(Contract.ForAll(source, x => x != null), "source's element is null");
      Contract.Requires<ArgumentException>(source.Min(x => x.Count()) == source.Max(x => x.Count()), "source has elements of different length");

      var enumerators = source.Select(x => x.GetEnumerator()).ToArray();
      try
      {
        while (enumerators.All(x => x.MoveNext()))
        {
          yield return enumerators.Select(x => elementSelector(x.Current)).ToArray();
        }
      }
      finally
      {
        foreach (var enumerator in enumerators)
          enumerator.Dispose();
      }
    }

    /// <summary>
    /// Transposes a m*n matrix of T's into a n*m matrix of T's
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="source">source matrix</param>
    /// <returns>Transposed matrix</returns>
    /// <exception cref="ArgumentNullException">Throws when source is null or one of source's elements is null</exception>
    /// <exception cref="ArgumentException">Throw when source is not m by n matrix</exception>
    [Pure]
    public static IEnumerable<IEnumerable<T>> Transpose<T>(this IEnumerable<IEnumerable<T>> source)
    {
      Contract.Requires<ArgumentNullException>(source != null, "source is null");
      Contract.Requires<ArgumentNullException>(Contract.ForAll(source, x => x != null), "source's element is null");
      Contract.Requires<ArgumentException>(source.Min(x => x.Count()) == source.Max(x => x.Count()), "source has elements of different length");

      return source.Transpose(x => x);
    }

    [Pure]
    public static IEnumerable<int> Range(int start, Func<int, bool> condition, Func<int, int> incrementFunc)
    {
      Contract.Requires<ArgumentNullException>(condition != null);
      Contract.Requires<ArgumentNullException>(incrementFunc != null);

      for (var i = start; condition(i); i = incrementFunc(i))
      {
        yield return i;
      }
    }

    [Pure]
    public static IEnumerable<int> Range(int start, Func<int, bool> condition, int increment)
    {
      Contract.Requires<ArgumentNullException>(condition != null);

      return Range(start, condition, i => i += increment);
    }

    [Pure]
    public static IEnumerable<long> Range(long start, Func<long, bool> condition, Func<long, long> incrementFunc)
    {
      Contract.Requires<ArgumentNullException>(condition != null);
      Contract.Requires<ArgumentNullException>(incrementFunc != null);

      for (var i = start; condition(i); i = incrementFunc(i))
      {
        yield return i;
      }
    }

    [Pure]
    public static IEnumerable<long> Range(long start, Func<long, bool> condition, long increment)
    {
      Contract.Requires<ArgumentNullException>(condition != null);

      return Range(start, condition, i => i += increment);
    }

    [Pure]
    public static IEnumerable<decimal> Range(decimal start, Func<decimal, bool> condition, Func<decimal, decimal> incrementFunc)
    {
      Contract.Requires<ArgumentNullException>(condition != null);
      Contract.Requires<ArgumentNullException>(incrementFunc != null);

      for (var i = start; condition(i); i = incrementFunc(i))
      {
        yield return i;
      }
    }

    [Pure]
    public static IEnumerable<decimal> Range(decimal start, Func<decimal, bool> condition, decimal increment)
    {
      Contract.Requires<ArgumentNullException>(condition != null);

      return Range(start, condition, i => i += increment);
    }

    [Pure]
    public static IEnumerable<T> Range<T>(T start, Func<T, bool> condition, Func<T, T> incrementFunc)
    {
      Contract.Requires<ArgumentNullException>(condition != null);
      Contract.Requires<ArgumentNullException>(incrementFunc != null);

      for (var i = start; condition(i); i = incrementFunc(i))
      {
        yield return i;
      }
    }

    /// <summary>
    /// Agregates source sequence returning each intermediate result, including initialState as first item
    /// </summary>
    /// <typeparam name="T">input element's type</typeparam>
    /// <typeparam name="U">return type</typeparam>
    /// <param name="input">input sequence</param>
    /// <param name="initialState">initial state, returned as first item</param>
    /// <param name="next">function(state, item, result)</param>
    /// <returns>function's value at each iteration</returns>
    /// <remarks>resulting sequence is 1 element longer than source sequence</remarks>
    [Pure]
    public static IEnumerable<U> Scan0<T, U>(this IEnumerable<T> input, U initialState, Func<U, T, U> next)
    {
      Contract.Requires<ArgumentNullException>(input != null);
      Contract.Requires<ArgumentNullException>(next != null);
      Contract.Ensures(Contract.Result<IEnumerable<U>>().Count() == input.Count() + 1);

      yield return initialState;
      foreach (var item in input)
      {
        initialState = next(initialState, item);
        yield return initialState;
      }
    }

    [Pure]
    public static IEnumerable<T> PadStart<T>(this IEnumerable<T> source, int padCount, T defaultPadValue = default(T))
    {
      Contract.Requires<ArgumentNullException>(source != null);
      Contract.Requires<ArgumentOutOfRangeException>(padCount >= 0);
      Contract.Ensures(Contract.Result<IEnumerable<T>>().Count() == source.Count() + padCount);

      return Enumerable.Repeat(defaultPadValue, padCount).Concat(source);
    }

    [Pure]
    public static IEnumerable<T> PadEnd<T>(this IEnumerable<T> source, int padCount, T defaultPadValue = default(T))
    {
      Contract.Requires<ArgumentNullException>(source != null);
      Contract.Requires<ArgumentOutOfRangeException>(padCount >= 0);
      Contract.Ensures(Contract.Result<IEnumerable<T>>().Count() == source.Count() + padCount);

      return source.Concat(Enumerable.Repeat(defaultPadValue, padCount));
    }
  }
}