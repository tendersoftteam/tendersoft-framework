﻿using System.Diagnostics.Contracts;

namespace System.Threading.Tasks.Tendersoft
{
  public static class TaskEx
  {
    //public static Task RunStaTask(Action action)
    //{
    //    return RunStaTask(action, SynchronizationContext.Current);
    //}

    public static Task RunStaTask(Action action)//, SynchronizationContext context)
    {
      Contract.Requires(action != null);

      var t = Task.Factory.StartNew(() =>
      {
        var thread = new System.Threading.Thread(() =>
        {
          action();
        });
        thread.Name = "void StaTaskThread";
        thread.SetApartmentState(System.Threading.ApartmentState.STA);
        thread.Start();
      });
      return t;
    }

    //public static Task<T> RunStaTask<T>(Func<T> func)
    //{
    //    return RunStaTask(func, SynchronizationContext.Current);
    //}
    public static Task<T> RunStaTask<T>(Func<T> func)//, SynchronizationContext context)
    {
      Contract.Requires(func != null);

      var task = Task.Factory.StartNew(() =>
      {
        T retVal = default(T);
        var thread = new System.Threading.Thread(() => { retVal = func(); });
        thread.Name = "non-void StaTaskThread";
        thread.SetApartmentState(System.Threading.ApartmentState.STA);
        thread.Start();

        thread.Join();
        return retVal;
      });
      return task;
    }

    public static void Sleep(this Task t, TimeSpan timeout)
    {
      //if (t != null && !t.IsCanceled && !t.IsCompleted && !t.IsFaulted)
      //{
      //  t.Wait(timeout);
      //}
      //else
      //{
      Sleep(timeout);
      //}
    }

    public static void Sleep(TimeSpan timeout)
    {
      SpinWait.SpinUntil(() => false, timeout);
      //var manualResetEvent = new ManualResetEventSlim();
      ////var threadTimer = new Timer(x => manualResetEvent.Set(),null,timeout,TimeSpan.MaxValue);
      //manualResetEvent.Wait(timeout);
      ////var hangedTask = new Task(() => Thread.SpinWait(int.MaxValue));
      ////Task.WaitAny(new Task[] { hangedTask }, timeout);
      ////await Task.Delay(1000);
    }

    public static Task Delay(double milliseconds)
    {
      var tcs = new TaskCompletionSource<bool>();
      System.Timers.Timer timer = new System.Timers.Timer();
      timer.Elapsed += (obj, args) =>
      {
        tcs.TrySetResult(true);
      };
      timer.Interval = milliseconds;
      timer.AutoReset = false;
      timer.Start();
      return tcs.Task;
    }
  }
}