﻿using System.Diagnostics.Contracts;

namespace System.Threading.Tendersoft
{
  public static class ThreadingExtensions
  {
    /// <summary>
    /// Executes function using EnterReadLock() and ExitReadLock() of <see cref="ReaderWriterLockSlim"/> class
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="readLock"><see cref="ReaderWriterLockSlim"/> instance used for locking</param>
    /// <param name="func"></param>
    /// <returns></returns>
    public static T ExecuteRead<T>(this ReaderWriterLockSlim readLock, Func<T> func)
    {
      Contract.Requires(readLock != null);
      Contract.Requires(func != null);

      try
      {
        readLock.EnterReadLock();
#if DEBUG
        var result = func();
        return result;
#else
								return func();
#endif
      }
      finally { readLock.ExitReadLock(); }
    }

    public static void ExecuteRead(this ReaderWriterLockSlim readLock, Action action)
    {
      Contract.Requires(readLock != null);
      Contract.Requires(action != null);

      readLock.ExecuteRead<int>(() => { action(); return 0; });
    }

    /// <summary>
    /// Executes function using EnterUpgradeableReadLock() and ExitUpgradeableReadLock() of <see cref="ReaderWriterLockSlim"/> class
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="upgradeLock"></param>
    /// <param name="func"></param>
    /// <returns></returns>
    public static T ExecuteUpgrade<T>(this ReaderWriterLockSlim upgradeLock, Func<T> func)
    {
      Contract.Requires(upgradeLock != null);
      Contract.Requires(func != null);

      try
      {
        upgradeLock.EnterUpgradeableReadLock();
#if DEBUG
        var result = func();
        return result;
#else
								return func();
#endif
      }
      finally { upgradeLock.ExitUpgradeableReadLock(); }
    }

    public static void ExecuteUpgrade(this ReaderWriterLockSlim upgradeLock, Action action)
    {
      Contract.Requires(upgradeLock != null);
      Contract.Requires(action != null);

      upgradeLock.ExecuteUpgrade(() => { action(); return 0; });
    }

    /// <summary>
    /// Executes function using EnterWriteLock() and ExitWriteLock() of <see cref="ReaderWriterLockSlim"/> class
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="writeLock"></param>
    /// <param name="func"></param>
    /// <returns></returns>
    public static T ExecuteWrite<T>(this ReaderWriterLockSlim writeLock, Func<T> func)
    {
      Contract.Requires(writeLock != null);
      Contract.Requires(func != null);

      try
      {
        writeLock.EnterWriteLock();
#if DEBUG
        var result = func();
        return result;
#else
								return func();
#endif
      }
      finally { writeLock.ExitWriteLock(); }
    }

    public static void ExecuteWrite(this ReaderWriterLockSlim writeLock, Action action)
    {
      Contract.Requires(writeLock != null);
      Contract.Requires(action != null);

      writeLock.ExecuteWrite(() => { action(); return 0; });
    }
  }
}