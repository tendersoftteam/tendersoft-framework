﻿using System.Threading.Tasks;

namespace System.Threading.Tendersoft
{
  public class ThreadEx
  {
    /// <summary>
    /// Executes function as a <typeparamref name="Task<T>"/>> and passed the return value to an <typeparamref name="Action<T>"/> on UI thread
    /// </summary>
    /// <typeparam name="T">Func return value type</typeparam>
    /// <param name="workFunc">Function to execute in background</param>
    /// <param name="uiAction">Action to perform on UI thread</param>
    /// <remarks>Method must be used from the UI thread</remarks>
    public static void AsyncUiWork<T>(Func<T> workFunc, Action<T> uiAction)
    {
      var ui = TaskScheduler.FromCurrentSynchronizationContext();
      var dataTask = Task.Factory.StartNew
      (() =>
          {
            return workFunc();
          }
      ).ContinueWith
      (t =>
          {
            uiAction(t.Result);
          }
          , ui
      );
    }
  }
}